package persist.factories;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import persist.Factory;
import persist.dao.DAOEndGame;

public class FactoryEndGamePlayers implements Factory<List<String>> {

	private UUID uuid;

	public FactoryEndGamePlayers(UUID uuid) {
		this.uuid = uuid;
	}

	@Override
	public List<String> create() {
		try {
			return DAOEndGame.getInstance().findPlayersByEndgameId(uuid);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
