package persist.factories;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import model.game.EndGame;
import persist.Factory;
import persist.dao.DAOPlayer;

public class FactoryPlayerEndGames implements Factory<List<EndGame>> {

	private UUID uuid;

	public FactoryPlayerEndGames(UUID uuid) {
		this.uuid = uuid;
	}

	@Override
	public List<EndGame> create() {
		try {
			return DAOPlayer.getInstance().findEndGamesByPlayerId(uuid);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
