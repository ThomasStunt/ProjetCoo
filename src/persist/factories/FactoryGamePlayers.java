package persist.factories;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import model.user.Player;
import persist.Factory;
import persist.dao.DAOGame;

public class FactoryGamePlayers implements Factory<List<Player>> {

	private UUID uuid;

	public FactoryGamePlayers(UUID uuid) {
		this.uuid = uuid;
	}

	@Override
	public List<Player> create() {
		try {
			return DAOGame.getInstance().findPlayersByGameId(uuid);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
