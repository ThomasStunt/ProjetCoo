package persist.factories;

import java.sql.SQLException;
import java.util.UUID;

import model.lands.Land;
import persist.Factory;
import persist.dao.DAOGameBoard;

public class FactoryGameBoardLands implements Factory<Land[][]> {

	private UUID uuid;

	public FactoryGameBoardLands(UUID uuid) {
		this.uuid = uuid;
	}

	@Override
	public Land[][] create() {
		try {
			return DAOGameBoard.getInstance().findLandsByGameBoardId(uuid);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
