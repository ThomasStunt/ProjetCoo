package persist.factories;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import model.user.Player;
import persist.Factory;
import persist.dao.DAOIteratListPlayer;

public class FactoryIteratListPlayerPlayers implements Factory<List<Player>> {

	private UUID uuid;

	public FactoryIteratListPlayerPlayers(UUID uuid) {
		this.uuid = uuid;
	}

	@Override
	public List<Player> create() {
		try {
			return DAOIteratListPlayer.getInstance().findPlayersByIteratListPlayerId(uuid);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
