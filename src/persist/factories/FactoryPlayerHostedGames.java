package persist.factories;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import model.game.Game;
import persist.Factory;
import persist.dao.DAOPlayer;

public class FactoryPlayerHostedGames implements Factory<List<Game>> {

	private UUID uuid;

	public FactoryPlayerHostedGames(UUID uuid) {
		this.uuid = uuid;
	}

	@Override
	public List<Game> create() {
		try {
			return DAOPlayer.getInstance().findHostedGamesByPlayerId(uuid);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
