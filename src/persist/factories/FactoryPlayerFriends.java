package persist.factories;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import model.user.Player;
import persist.Factory;
import persist.dao.DAOPlayer;

public class FactoryPlayerFriends implements Factory<List<Player>> {

	private UUID uuid;

	public FactoryPlayerFriends(UUID uuid) {
		this.uuid = uuid;
	}

	@Override
	public List<Player> create() {
		try {
			return DAOPlayer.getInstance().findFriendsByPlayerId(uuid);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
