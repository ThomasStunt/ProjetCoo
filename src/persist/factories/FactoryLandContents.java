package persist.factories;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import model.player.content.Content;
import persist.Factory;
import persist.dao.DAOContent;

public class FactoryLandContents implements Factory<List<Content>> {

	private UUID uuid;

	public FactoryLandContents(UUID uuid) {
		this.uuid = uuid;
	}

	@Override
	public List<Content> create() {
		try {
			return DAOContent.getInstance().findContentsByLandId(uuid);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
