package persist;

import java.sql.SQLException;
import java.util.UUID;

public interface DataMapper<T> {

	public T insert(T obj, Object... args) throws SQLException;

	public void delete(UUID id) throws SQLException;

	public void update(T obj, Object... args) throws SQLException;

	public T findById(UUID id) throws SQLException;

}
