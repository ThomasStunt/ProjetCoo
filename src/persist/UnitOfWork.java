package persist;

import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import model.IDomainObject;
import model.Visitor;

public class UnitOfWork implements Observer {

	/**
	 * Contains modified object compare to database
	 */
	Set<IDomainObject> dirty;

	/**
	 * Instance of this class
	 */
	private static UnitOfWork inst = null;

	/**
	 * @return the single instance of this class
	 */
	public static UnitOfWork getInstance() {
		if (inst == null)
			inst = new UnitOfWork();
		return inst;
	}

	/**
	 * Private constructor
	 */
	private UnitOfWork() {
		dirty = new HashSet<>();
	}

	/**
	 * For each modified object in {@link #dirty}, we save the modification into db
	 * with an {@link Updater}.
	 */
	public void commit() {
		Visitor v = new Updater();
		for (IDomainObject o : dirty) {
			v.visit(o);
		}
		dirty.clear();
	}

	/**
	 * Clear the {@link Set} {@link #dirty}
	 */
	public void rollbackAll() {
		dirty.clear();
	}

	/**
	 * If args o correspond to an {@link IDomainObject}, then add it in
	 * {@link #dirty}
	 */
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof IDomainObject) {
			dirty.add((IDomainObject) o);
		}
	}

}