package persist;

import java.sql.SQLException;

import model.Visitor;
import model.game.EndGame;
import model.game.Game;
import model.game.GameBoard;
import model.game.Settings;
import model.game.round.IteratListPlayer;
import model.game.round.Round;
import model.lands.Land;
import model.player.content.Content;
import model.user.Player;
import persist.dao.DAOContent;
import persist.dao.DAOEndGame;
import persist.dao.DAOGame;
import persist.dao.DAOGameBoard;
import persist.dao.DAOIteratListPlayer;
import persist.dao.DAOLand;
import persist.dao.DAOPlayer;
import persist.dao.DAORound;
import persist.dao.DAOSettings;

public class Updater extends Visitor {

	@Override
	public void visit(Player p) {
		try {
			DAOPlayer.getInstance().update(p);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void visit(EndGame eg) {
		try {
			DAOEndGame.getInstance().update(eg);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void visit(Game g) {
		try {
			DAOGame.getInstance().update(g);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void visit(GameBoard gb) {
		try {
			DAOGameBoard.getInstance().update(gb);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void visit(Settings s) {
		try {
			DAOSettings.getInstance().update(s);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void visit(IteratListPlayer ilp) {
		try {
			DAOIteratListPlayer.getInstance().update(ilp);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void visit(Round r) {
		try {
			DAORound.getInstance().update(r);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void visit(Land l) {
		try {
			DAOLand.getInstance().update(l);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void visit(Content c) {
		try {
			DAOContent.getInstance().update(c);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
