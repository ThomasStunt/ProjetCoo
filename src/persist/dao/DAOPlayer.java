package persist.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.WeakHashMap;

import model.game.EndGame;
import model.game.Game;
import model.user.Player;
import persist.DBConnection;
import persist.DataMapper;
import persist.UnitOfWork;
import persist.VirtualProxyBuilder;
import persist.factories.FactoryPlayerEndGames;
import persist.factories.FactoryPlayerFriends;
import persist.factories.FactoryPlayerGames;
import persist.factories.FactoryPlayerHostedGames;

public class DAOPlayer implements DataMapper<Player> {

	private WeakHashMap<UUID, Player> map;

	private static DAOPlayer inst = null;

	public static DAOPlayer getInstance() {
		if (inst == null)
			inst = new DAOPlayer();
		return inst;
	}

	private DAOPlayer() {
		super();
		this.map = new WeakHashMap<UUID, Player>();
	}

	@Override
	public Player insert(Player obj, Object... args) throws SQLException {
		String req1 = "INSERT INTO PROCOO_PLAYER (id, name, resources) VALUES (?,?,?)";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps1 = c.prepareStatement(req1);
		UUID uuid = UUID.randomUUID();
		ps1.setString(1, uuid.toString());
		ps1.setString(2, obj.getName());
		ps1.setInt(3, obj.getRessources());
		ps1.executeUpdate();

		obj.setId(uuid);
		obj.addObserver(UnitOfWork.getInstance());

		map.put(uuid, obj);

		insertPlayerFriends(obj, uuid);
		insertPlayerHostedGames(obj, uuid);
		insertPlayerGames(obj, uuid);
		insertPlayerEndgames(obj, uuid);

		return obj;
	}

	@Override
	public void delete(UUID id) throws SQLException {
		String req = "DELETE PROCOO_PLAYER WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		ps.setString(1, id.toString());
		ps.executeUpdate();

		if (map.containsKey(id) == true) {
			map.remove(id);
		}
	}

	@Override
	public void update(Player obj, Object... args) throws SQLException {
		String req1 = "UPDATE PROCOO_PLAYER SET name = ?, resources = ? WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps1 = c.prepareStatement(req1);
		ps1.setString(1, obj.getName());
		ps1.setInt(2, obj.getRessources());
		ps1.setString(3, obj.getId().toString());
		ps1.executeUpdate();

		String req2 = "DELETE PROCOO_PLAYER_FRIENDS WHERE id_source = ?";
		PreparedStatement ps2 = c.prepareStatement(req2);
		ps2.setString(1, obj.getId().toString());
		ps2.executeUpdate();

		insertPlayerFriends(obj, obj.getId());

		String req3 = "DELETE PROCOO_PLAYER_HOSTEDGAMES WHERE id_source_player = ?";
		PreparedStatement ps3 = c.prepareStatement(req3);
		ps3.setString(1, obj.getId().toString());
		ps3.executeUpdate();

		insertPlayerHostedGames(obj, obj.getId());

		String req4 = "DELETE PROCOO_PLAYER_GAMES WHERE id_source_player = ?";
		PreparedStatement ps4 = c.prepareStatement(req4);
		ps4.setString(1, obj.getId().toString());
		ps4.executeUpdate();

		insertPlayerGames(obj, obj.getId());

		String req5 = "DELETE PROCOO_PLAYER_ENDGAMES WHERE id_source_player = ?";
		PreparedStatement ps5 = c.prepareStatement(req5);
		ps5.setString(1, obj.getId().toString());
		ps5.executeUpdate();

		insertPlayerEndgames(obj, obj.getId());

	}

	private void insertPlayerFriends(Player obj, UUID uuid) throws SQLException {
		String req = "INSERT INTO PROCOO_PLAYER_FRIENDS (id_source, id_target) VALUES (?,?)";
		Connection c = DBConnection.getConnection();
		for (Player player : obj.getFriends()) {
			PreparedStatement ps = c.prepareStatement(req);
			ps.setString(1, uuid.toString());
			ps.setString(2, player.getId().toString());
			ps.executeUpdate();
		}
	}

	private void insertPlayerHostedGames(Player obj, UUID uuid) throws SQLException {
		String req = "INSERT INTO PROCOO_PLAYER_HOSTEDGAMES (id_source_player, id_target_game) VALUES (?,?)";
		Connection c = DBConnection.getConnection();
		for (Game game : obj.getHostedGames()) {
			PreparedStatement ps = c.prepareStatement(req);
			ps.setString(1, uuid.toString());
			ps.setString(2, game.getId().toString());
			ps.executeUpdate();
		}
	}

	private void insertPlayerGames(Player obj, UUID uuid) throws SQLException {
		String req = "INSERT INTO PROCOO_PLAYER_GAMES (id_source_player, id_target_game) VALUES (?,?)";
		Connection c = DBConnection.getConnection();
		for (Game game : obj.getGames()) {
			PreparedStatement ps = c.prepareStatement(req);
			ps.setString(1, uuid.toString());
			ps.setString(2, game.getId().toString());
			ps.executeUpdate();
		}
	}

	private void insertPlayerEndgames(Player obj, UUID uuid) throws SQLException {
		String req = "INSERT INTO PROCOO_PLAYER_ENDGAMES (id_source_player, id_target_endgame) VALUES (?,?)";
		Connection c = DBConnection.getConnection();
		for (EndGame game : obj.getEndGames()) {
			PreparedStatement ps = c.prepareStatement(req);
			ps.setString(1, uuid.toString());
			ps.setString(2, game.getId().toString());
			ps.executeUpdate();
		}
	}

	public Player findByUniqueName(String name) throws SQLException {
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, name, resources FROM PROCOO_PLAYER WHERE name = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, name);
		ResultSet result = stmt.executeQuery();
		result.next();
		UUID uuid = UUID.fromString(result.getString(1));
		if (map.containsKey(uuid) == true) {
			return map.get(uuid);
		}
		String fname = result.getString(2);
		Integer resources = result.getInt(3);
		Player p = new Player(fname, resources);
		p.setId(uuid);
		p.addObserver(UnitOfWork.getInstance());
		p.setFriends(new VirtualProxyBuilder<List<Player>>(List.class, new FactoryPlayerFriends(uuid)).getProxy());
		p.setHostedGames(
				new VirtualProxyBuilder<List<Game>>(List.class, new FactoryPlayerHostedGames(uuid)).getProxy());
		p.setGames(new VirtualProxyBuilder<List<Game>>(List.class, new FactoryPlayerGames(uuid)).getProxy());
		p.setEndGames(new VirtualProxyBuilder<List<EndGame>>(List.class, new FactoryPlayerEndGames(uuid)).getProxy());
		map.put(uuid, p);
		return p;
	}

	@Override
	public Player findById(UUID id) throws SQLException {
		if (map.containsKey(id) == true) {
			return map.get(id);
		}
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, name, resources FROM PROCOO_PLAYER WHERE id = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		result.next();
		String name = result.getString(2);
		Integer resources = result.getInt(3);
		Player p = new Player(name, resources);
		p.setId(id);
		p.addObserver(UnitOfWork.getInstance());
		p.setFriends(new VirtualProxyBuilder<List<Player>>(List.class, new FactoryPlayerFriends(id)).getProxy());
		p.setHostedGames(new VirtualProxyBuilder<List<Game>>(List.class, new FactoryPlayerHostedGames(id)).getProxy());
		p.setGames(new VirtualProxyBuilder<List<Game>>(List.class, new FactoryPlayerGames(id)).getProxy());
		p.setEndGames(new VirtualProxyBuilder<List<EndGame>>(List.class, new FactoryPlayerEndGames(id)).getProxy());
		map.put(id, p);
		return p;
	}

	public List<Player> findFriendsByPlayerId(UUID id) throws SQLException {
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id_target FROM PROCOO_PLAYER_FRIENDS WHERE id_source = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		List<Player> players = new ArrayList<>();
		while (result.next()) {
			UUID tmpId = UUID.fromString(result.getString(1));
			Player tmp = findById(tmpId);
			players.add(tmp);
		}
		return players;
	}

	public List<Game> findHostedGamesByPlayerId(UUID id) throws SQLException {
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id_target_game FROM PROCOO_PLAYER_HOSTEDGAMES WHERE id_source_player = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		List<Game> hostedGames = new ArrayList<>();
		while (result.next()) {
			UUID tmpId = UUID.fromString(result.getString(1));
			Game tmp = DAOGame.getInstance().findById(tmpId);
			hostedGames.add(tmp);
		}
		return hostedGames;
	}

	public List<Game> findGamesByPlayerId(UUID id) throws SQLException {
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id_target_game FROM PROCOO_PLAYER_GAMES WHERE id_source_player = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		List<Game> games = new ArrayList<>();
		while (result.next()) {
			UUID tmpId = UUID.fromString(result.getString(1));
			Game tmp = DAOGame.getInstance().findById(tmpId);
			games.add(tmp);
		}
		return games;
	}

	public List<EndGame> findEndGamesByPlayerId(UUID id) throws SQLException {
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id_target_endgame FROM PROCOO_PLAYER_ENDGAMES WHERE id_source_player = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		List<EndGame> endgames = new ArrayList<>();
		while (result.next()) {
			UUID tmpId = UUID.fromString(result.getString(1));
			EndGame tmp = DAOEndGame.getInstance().findById(tmpId);
			endgames.add(tmp);
		}
		return endgames;
	}

}
