package persist.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.WeakHashMap;

import model.game.GameBoard;
import model.game.Settings;
import model.lands.Land;
import persist.DBConnection;
import persist.DataMapper;
import persist.UnitOfWork;

public class DAOGameBoard implements DataMapper<GameBoard> {

	private WeakHashMap<UUID, GameBoard> map;

	private static DAOGameBoard inst = null;

	public static DAOGameBoard getInstance() {
		if (inst == null)
			inst = new DAOGameBoard();
		return inst;
	}

	private DAOGameBoard() {
		super();
		this.map = new WeakHashMap<UUID, GameBoard>();
	}

	@Override
	public GameBoard insert(GameBoard obj, Object... args) throws SQLException {
		String req = "INSERT INTO PROCOO_GAMEBOARD (id, sizeX, sizeY, id_game) VALUES (?,?,?,?)";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		UUID uuid = UUID.randomUUID();
		try {
			UUID gameId = (UUID) args[0];
			ps.setString(4, gameId.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			ps.setNull(4, java.sql.Types.VARCHAR);
		}
		ps.setString(1, uuid.toString());
		ps.setInt(2, obj.getSizeX());
		ps.setInt(3, obj.getSizeY());
		ps.executeUpdate();

		obj.setId(uuid);
		obj.addObserver(UnitOfWork.getInstance());

		map.put(uuid, obj);

		for (int i = 0; i < obj.getSizeX(); i++) {
			for (int j = 0; j < obj.getSizeX(); j++) {
				DAOLand.getInstance().insert(obj.getMap()[i][j], obj.getId());
			}
		}
		return obj;
	}

	@Override
	public void delete(UUID id) throws SQLException {
		String req = "DELETE PROCOO_GAMEBOARD WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		ps.setString(1, id.toString());
		ps.executeUpdate();

		if (map.containsKey(id) == true) {
			map.remove(id);
		}

	}

	@Override
	public void update(GameBoard obj, Object... args) throws SQLException {
		String req = "UPDATE PROCOO_GAMEBOARD SET sizeX = ?, sizeY = ?, id_game = ? WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps1 = c.prepareStatement(req);
		ps1.setInt(1, obj.getSizeX());
		ps1.setInt(2, obj.getSizeY());
		try {
			UUID gameId = (UUID) args[0];
			ps1.setString(3, gameId.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			ps1.setNull(3, java.sql.Types.VARCHAR);
		}
		ps1.setString(5, obj.getId().toString());
		ps1.executeUpdate();

		for (int i = 0; i < obj.getSizeX(); i++) {
			for (int j = 0; j < obj.getSizeX(); j++) {
				DAOLand.getInstance().update(obj.getMap()[i][j], obj.getId());
			}
		}

	}

	@Override
	public GameBoard findById(UUID id) throws SQLException {
		if (map.containsKey(id) == true) {
			return map.get(id);
		}
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, sizeX, sizeY, id_game FROM PROCOO_GAMEBOARD WHERE id = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		result.next();
		UUID id_game = UUID.fromString(result.getString(4));
		Settings settings = DAOSettings.getInstance().findByUniqueGameId(id_game);
		GameBoard gameBoard = new GameBoard(settings);
		gameBoard.setId(id);
		gameBoard.addObserver(UnitOfWork.getInstance());
		gameBoard.setMap(DAOLand.getInstance().findByGameBoardId(id));
		map.put(id, gameBoard);
		return gameBoard;
	}

	public Land[][] findLandsByGameBoardId(UUID id) throws SQLException {
		GameBoard gb = findById(id);
		Land[][] lands = new Land[gb.getSizeX()][gb.getSizeY()];
		List<Land> listLands = DAOLand.getInstance().findByGameBoardId(id);
		for (Land land : listLands) {
			lands[land.getX()][land.getY()] = land;
		}
		return lands;
	}

	public GameBoard findByUniqueGameId(UUID id) throws SQLException {
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, sizeX, sizeY, id_game FROM PROCOO_GAMEBOARD WHERE id_game = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		result.next();
		UUID id_gb = UUID.fromString(result.getString(1));
		if (map.containsKey(id_gb) == true) {
			return map.get(id_gb);
		}
		UUID id_game = UUID.fromString(result.getString(4));
		Settings settings = DAOSettings.getInstance().findByUniqueGameId(id_game);
		GameBoard gameBoard = new GameBoard(settings);
		gameBoard.setId(id_gb);
		gameBoard.addObserver(UnitOfWork.getInstance());
		gameBoard.setMap(DAOLand.getInstance().findByGameBoardId(id_gb));
		map.put(id_gb, gameBoard);
		return gameBoard;
	}

}
