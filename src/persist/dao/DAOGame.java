package persist.dao;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.WeakHashMap;

import model.game.Game;
import model.game.GameBoard;
import model.game.Settings;
import model.game.round.Round;
import model.game.state.GameState;
import model.user.Player;
import persist.DBConnection;
import persist.DataMapper;
import persist.UnitOfWork;
import persist.VirtualProxyBuilder;
import persist.factories.FactoryGamePlayers;

public class DAOGame implements DataMapper<Game> {

	private WeakHashMap<UUID, Game> map;

	private static DAOGame inst = null;

	public static DAOGame getInstance() {
		if (inst == null)
			inst = new DAOGame();
		return inst;
	}

	private DAOGame() {
		super();
		this.map = new WeakHashMap<UUID, Game>();
	}

	@Override
	public Game insert(Game obj, Object... args) throws SQLException {
		String req = "INSERT INTO PROCOO_GAME (id, name, playerHost, state) VALUES (?,?,?,?)";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		UUID uuid = UUID.randomUUID();
		ps.setString(1, uuid.toString());
		ps.setString(2, obj.getName());
		ps.setString(3, obj.getHost().getId().toString());
		ps.setString(4, obj.getState().getState());
		ps.executeUpdate();

		obj.setId(uuid);

		if (obj.getGameBoard() != null) {
			DAOGameBoard.getInstance().insert(obj.getGameBoard(), obj.getId());
		}
		if (obj.getSettings() != null) {
			DAOSettings.getInstance().insert(obj.getSettings(), obj.getId());
		}
		if (obj.getRound() != null) {
			DAORound.getInstance().insert(obj.getRound(), obj.getId());
		}

		insertAssoGamePlayers(obj, obj.getId());

		obj.addObserver(UnitOfWork.getInstance());

		map.put(uuid, obj);

		return obj;
	}

	@Override
	public void delete(UUID id) throws SQLException {
		String req = "DELETE PROCOO_GAME WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		ps.setString(1, id.toString());
		ps.executeUpdate();

		if (map.containsKey(id) == true) {
			map.remove(id);
		}

	}

	@Override
	public void update(Game obj, Object... args) throws SQLException {
		String req = "UPDATE PROCOO_GAME SET name = ?, playerHost = ?, state = ? WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps1 = c.prepareStatement(req);
		ps1.setString(1, obj.getName());
		ps1.setString(2, obj.getHost().getId().toString());
		ps1.setString(3, obj.getState().getState());
		ps1.setString(4, obj.getId().toString());
		ps1.executeUpdate();
		if (obj.getGameBoard() != null) {
			DAOGameBoard.getInstance().update(obj.getGameBoard(), obj.getId());
		}
		if (obj.getSettings() != null) {
			DAOSettings.getInstance().update(obj.getSettings(), obj.getId());
		}
		if (obj.getRound() != null) {
			DAORound.getInstance().update(obj.getRound(), obj.getId());
		}

		String req2 = "DELETE PROCOO_GAME_PLAYERS WHERE id_game = ?";
		PreparedStatement ps2 = c.prepareStatement(req2);
		ps2.setString(1, obj.getId().toString());
		ps2.executeUpdate();

		insertAssoGamePlayers(obj, obj.getId());
	}

	private void insertAssoGamePlayers(Game obj, UUID uuid) throws SQLException {
		String req = "INSERT INTO PROCOO_GAME_PLAYERS (id_game, id_player) VALUES (?,?)";
		Connection c = DBConnection.getConnection();
		for (Player player : obj.getPlayers()) {
			PreparedStatement ps = c.prepareStatement(req);
			ps.setString(1, uuid.toString());
			ps.setString(2, player.getId().toString());
			ps.executeUpdate();
		}
	}

	@Override
	public Game findById(UUID id) throws SQLException {
		if (map.containsKey(id) == true) {
			return map.get(id);
		}
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, name, playerHost, state FROM PROCOO_GAME WHERE id = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		result.next();
		String name = result.getString(2);
		String id_playerHost = result.getString(3);
		String state = result.getString(4);
		Player host = DAOPlayer.getInstance().findById(UUID.fromString(id_playerHost));
		Settings settings = DAOSettings.getInstance().findByUniqueGameId(id);
		GameBoard gameBoard = null;
		Round round = null;
		try {
			gameBoard = DAOGameBoard.getInstance().findByUniqueGameId(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			round = DAORound.getInstance().findByUniqueGameId(id);
		} catch (SQLException e) {
			// TODO: handle exception
		}

		String stateClassName = state.substring(0, 1) + state.substring(1).toLowerCase() + "State";
		Object o = null;
		try {
			Class<?> stateClass = Class.forName("model.game.state." + stateClassName);
			Constructor<?> constr;
			constr = stateClass.getConstructor();
			o = constr.newInstance();
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException | ClassNotFoundException e) {
			throw new SQLException(e);
		}

		Game g = new Game(name, host, settings);
		g.setId(id);
		g.addObserver(UnitOfWork.getInstance());
		g.setState((GameState) o);
		g.setGameBoard(gameBoard);
		g.setRound(round);
		g.setPlayers(new VirtualProxyBuilder<List<Player>>(List.class, new FactoryGamePlayers(id)).getProxy());
		map.put(id, g);
		return g;
	}

	public List<Player> findPlayersByGameId(UUID id) throws SQLException {
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id_player FROM PROCOO_GAME_PLAYERS WHERE id_game = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		List<Player> players = new ArrayList<>();
		while (result.next()) {
			UUID tmpId = UUID.fromString(result.getString(1));
			Player tmp = DAOPlayer.getInstance().findById(tmpId);
			players.add(tmp);
		}
		return players;
	}

}
