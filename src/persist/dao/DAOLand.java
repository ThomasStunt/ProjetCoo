package persist.dao;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.WeakHashMap;

import model.lands.Land;
import model.player.content.Content;
import persist.DBConnection;
import persist.DataMapper;
import persist.UnitOfWork;
import persist.VirtualProxyBuilder;
import persist.factories.FactoryLandContents;

public class DAOLand implements DataMapper<Land> {

	private WeakHashMap<UUID, Land> map;

	private static DAOLand inst = null;

	public static DAOLand getInstance() {
		if (inst == null)
			inst = new DAOLand();
		return inst;
	}

	private DAOLand() {
		super();
		this.map = new WeakHashMap<UUID, Land>();
	}

	@Override
	public Land insert(Land obj, Object... args) throws SQLException {
		String req = "INSERT INTO PROCOO_LAND (id, loc_x, loc_y, type, id_gameboard) VALUES (?,?,?,?,?)";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		UUID uuid = UUID.randomUUID();
		try {
			UUID gameboardId = (UUID) args[0];
			ps.setString(5, gameboardId.toString());
		} catch (ClassCastException | ArrayIndexOutOfBoundsException e) {
			ps.setNull(5, java.sql.Types.VARCHAR);
		}
		ps.setString(1, uuid.toString());
		ps.setInt(2, obj.getX());
		ps.setInt(3, obj.getY());
		ps.setString(4, obj.getType());
		ps.executeUpdate();

		obj.setId(uuid);
		obj.addObserver(UnitOfWork.getInstance());

		map.put(uuid, obj);

		for (Content content : obj.getContents()) {
			DAOContent.getInstance().insert(content, obj.getId());
		}

		return obj;
	}

	@Override
	public void delete(UUID id) throws SQLException {
		String req = "DELETE PROCOO_LAND WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		ps.setString(1, id.toString());
		ps.executeUpdate();

		if (map.containsKey(id) == true) {
			map.remove(id);
		}

	}

	@Override
	public void update(Land obj, Object... args) throws SQLException {
		String req = "UPDATE PROCOO_LAND SET loc_x = ?, loc_y = ?, type = ?, id_gameboard = ? WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps1 = c.prepareStatement(req);
		ps1.setInt(1, obj.getX());
		ps1.setInt(2, obj.getY());
		ps1.setString(3, obj.getType());
		try {
			UUID gameboardId = (UUID) args[0];
			ps1.setString(4, gameboardId.toString());
		} catch (ClassCastException | ArrayIndexOutOfBoundsException e) {
			ps1.setNull(4, java.sql.Types.VARCHAR);
		}
		ps1.setString(5, obj.getId().toString());
		ps1.executeUpdate();

		for (Content content : obj.getContents()) {
			DAOContent.getInstance().update(content, obj.getId());
		}
	}

	@Override
	public Land findById(UUID id) throws SQLException {
		if (map.containsKey(id) == true) {
			return map.get(id);
		}
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, loc_x, loc_y, type, id_gameboard FROM PROCOO_LAND WHERE id = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		result.next();
		Integer loc_x = result.getInt(2);
		Integer loc_y = result.getInt(3);
		String type = result.getString(4);
		String className = type.substring(0, 1) + type.substring(1).toLowerCase() + "Land";
		Object o = null;
		try {
			Class<?> contentClass = Class.forName("model.lands." + className);
			Constructor<?> constr;
			constr = contentClass.getConstructor(int.class, int.class);
			o = constr.newInstance(loc_x, loc_y);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException | ClassNotFoundException e) {
			throw new SQLException(e);
		}
		Land land = (Land) o;
		land.setId(id);
		land.addObserver(UnitOfWork.getInstance());
		land.setContents(new VirtualProxyBuilder<List<Content>>(List.class, new FactoryLandContents(id)).getProxy());
		map.put(id, land);
		return land;
	}

	public List<Land> findByGameBoardId(UUID id) throws SQLException {
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, loc_x, loc_y, type, id_gameboard FROM PROCOO_LAND WHERE id_gameboard = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		List<Land> lands = new ArrayList<>();
		while (result.next()) {
			UUID id_land = UUID.fromString(result.getString(1));
			Land tmpLand = findById(id_land);
			lands.add(tmpLand);
		}
		return lands;
	}

}
