package persist.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.WeakHashMap;

import model.game.round.Round;
import persist.DBConnection;
import persist.DataMapper;
import persist.UnitOfWork;

public class DAORound implements DataMapper<Round> {

	private WeakHashMap<UUID, Round> map;

	private static DAORound inst = null;

	public static DAORound getInstance() {
		if (inst == null)
			inst = new DAORound();
		return inst;
	}

	private DAORound() {
		super();
		this.map = new WeakHashMap<UUID, Round>();
	}

	@Override
	public Round insert(Round obj, Object... args) throws SQLException {
		String req = "INSERT INTO PROCOO_ROUND (id, numberRound, id_game) VALUES (?,?,?)";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		UUID uuid = UUID.randomUUID();
		ps.setString(1, uuid.toString());
		ps.setInt(2, obj.getNumberRound());

		try {
			UUID id_game = (UUID) args[0];
			ps.setString(3, id_game.toString());
		} catch (ClassCastException | ArrayIndexOutOfBoundsException e) {
			ps.setNull(3, java.sql.Types.VARCHAR);
		}

		ps.executeUpdate();

		obj.setId(uuid);
		obj.addObserver(UnitOfWork.getInstance());

		map.put(uuid, obj);

		if (obj.getPlayersOrder() != null) {
			DAOIteratListPlayer.getInstance().insert(obj.getPlayersOrder(), obj.getId());
		}

		return obj;
	}

	@Override
	public void delete(UUID id) throws SQLException {
		String req = "DELETE PROCOO_ROUND WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		ps.setString(1, id.toString());
		ps.executeUpdate();

		if (map.containsKey(id) == true) {
			map.remove(id);
		}
	}

	@Override
	public void update(Round obj, Object... args) throws SQLException {
		String req = "UPDATE PROCOO_ROUND SET numberRound = ?, id_game = ? WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps1 = c.prepareStatement(req);
		ps1.setInt(1, obj.getNumberRound());
		ps1.setString(3, obj.getId().toString());
		try {
			UUID id_game = (UUID) args[0];
			ps1.setString(2, id_game.toString());
		} catch (ClassCastException | ArrayIndexOutOfBoundsException e) {
			ps1.setNull(2, java.sql.Types.VARCHAR);
		}
		ps1.executeUpdate();

		if (obj.getPlayersOrder() != null) {
			DAOIteratListPlayer.getInstance().update(obj.getPlayersOrder(), obj.getId());
		}
	}

	@Override
	public Round findById(UUID id) throws SQLException {
		if (map.containsKey(id) == true) {
			return map.get(id);
		}
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, numberRound, id_game FROM PROCOO_ROUND WHERE id = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		result.next();
		Integer numberRound = result.getInt(2);
		Round round = new Round(numberRound, null);
		round.setId(id);
		round.addObserver(UnitOfWork.getInstance());
		round.setPlayersOrder(DAOIteratListPlayer.getInstance().findByUniqueIdRound(id));
		map.put(id, round);
		return round;
	}

	public Round findByUniqueGameId(UUID id) throws SQLException {
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, numberRound, id_game FROM PROCOO_ROUND WHERE id_game = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		result.next();
		UUID id_round = UUID.fromString(result.getString(1));
		if (map.containsKey(id_round) == true) {
			return map.get(id_round);
		}
		Integer numberRound = result.getInt(2);
		Round round = new Round(numberRound, null);
		round.setId(id_round);
		round.addObserver(UnitOfWork.getInstance());
		round.setPlayersOrder(DAOIteratListPlayer.getInstance().findByUniqueIdRound(id));
		map.put(id_round, round);
		return round;
	}

}
