package persist.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.WeakHashMap;

import model.game.round.IteratListPlayer;
import model.user.Player;
import persist.DBConnection;
import persist.DataMapper;
import persist.UnitOfWork;
import persist.VirtualProxyBuilder;
import persist.factories.FactoryIteratListPlayerPlayers;

public class DAOIteratListPlayer implements DataMapper<IteratListPlayer> {

	private WeakHashMap<UUID, IteratListPlayer> map;

	private static DAOIteratListPlayer inst = null;

	public static DAOIteratListPlayer getInstance() {
		if (inst == null)
			inst = new DAOIteratListPlayer();
		return inst;
	}

	private DAOIteratListPlayer() {
		super();
		this.map = new WeakHashMap<UUID, IteratListPlayer>();
	}

	@Override
	public IteratListPlayer insert(IteratListPlayer obj, Object... args) throws SQLException {
		String req = "INSERT INTO PROCOO_ITERAPLAYERROUND (id, indexList, id_round) VALUES (?,?,?)";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		UUID uuid = UUID.randomUUID();
		try {
			UUID roundId = (UUID) args[0];
			ps.setString(3, roundId.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			ps.setNull(3, java.sql.Types.VARCHAR);
		}
		ps.setString(1, uuid.toString());
		ps.setInt(2, obj.getIndex());
		ps.executeUpdate();

		obj.setId(uuid);
		obj.addObserver(UnitOfWork.getInstance());

		map.put(uuid, obj);

		insertAssoIteraPlayer(obj, uuid);

		return obj;
	}

	@Override
	public void delete(UUID id) throws SQLException {
		String req = "DELETE PROCOO_ITERAPLAYERROUND WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		ps.setString(1, id.toString());
		ps.executeUpdate();

		if (map.containsKey(id) == true) {
			map.remove(id);
		}

	}

	@Override
	public void update(IteratListPlayer obj, Object... args) throws SQLException {
		String req1 = "UPDATE PROCOO_ITERAPLAYERROUND SET indexList = ?, id_round WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps1 = c.prepareStatement(req1);
		ps1.setInt(1, obj.getIndex());
		try {
			UUID roundId = (UUID) args[0];
			ps1.setString(2, roundId.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			ps1.setNull(2, java.sql.Types.VARCHAR);
		}
		ps1.setString(3, obj.getId().toString());
		ps1.executeUpdate();

		String req2 = "DELETE PROCOO_ITERAPLAYEROUND_PLAYER WHERE id_itera = ?";
		PreparedStatement ps2 = c.prepareStatement(req2);
		ps2.setString(1, obj.getId().toString());
		ps2.executeUpdate();

		insertAssoIteraPlayer(obj, obj.getId());

	}

	private void insertAssoIteraPlayer(IteratListPlayer obj, UUID uuid) throws SQLException {
		String req = "INSERT INTO PROCOO_ITERAPLAYEROUND_PLAYER (id_itera, id_player, porder) VALUES (?,?,?)";
		Connection c = DBConnection.getConnection();
		int i = 0;
		for (Player player : obj.getPlayers()) {
			PreparedStatement ps = c.prepareStatement(req);
			ps.setString(1, uuid.toString());
			ps.setString(2, player.getId().toString());
			ps.setInt(3, i);
			ps.executeUpdate();
			i++;
		}
	}

	@Override
	public IteratListPlayer findById(UUID id) throws SQLException {
		if (map.containsKey(id) == true) {
			return map.get(id);
		}
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, indexList, id_round FROM PROCOO_ITERAPLAYERROUND WHERE id = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		result.next();
		Integer indexList = result.getInt(2);
		IteratListPlayer ilp = new IteratListPlayer(null);
		ilp.setId(id);
		ilp.addObserver(UnitOfWork.getInstance());
		ilp.setIndex(indexList);
		ilp.setPlayers(
				new VirtualProxyBuilder<List<Player>>(List.class, new FactoryIteratListPlayerPlayers(id)).getProxy());
		map.put(id, ilp);
		return ilp;
	}

	public IteratListPlayer findByUniqueIdRound(UUID id) throws SQLException {
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, indexList, id_round FROM PROCOO_ITERAPLAYERROUND WHERE id_round = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		result.next();
		UUID id_itera = UUID.fromString(result.getString(1));
		if (map.containsKey(id_itera) == true) {
			return map.get(id_itera);
		}
		Integer indexList = result.getInt(2);
		IteratListPlayer ilp = new IteratListPlayer(null);
		ilp.setId(id);
		ilp.addObserver(UnitOfWork.getInstance());
		ilp.setIndex(indexList);
		ilp.setPlayers(
				new VirtualProxyBuilder<List<Player>>(List.class, new FactoryIteratListPlayerPlayers(id)).getProxy());
		map.put(id_itera, ilp);
		return ilp;
	}

	public List<Player> findPlayersByIteratListPlayerId(UUID id) throws SQLException {
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id_itera, id_player, porder FROM PROCOO_ITERAPLAYEROUND_PLAYER WHERE id_itera = ? ORDER BY porder ASC";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		List<Player> players = new ArrayList<>();
		while (result.next()) {
			UUID id_player = UUID.fromString(result.getString(2));
			Player p = DAOPlayer.getInstance().findById(id_player);
			players.add(p);
		}
		return null;
	}

}
