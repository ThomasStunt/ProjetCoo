package persist.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.WeakHashMap;

import model.game.Settings;
import model.lands.Land;
import model.user.player.actions.AbstractAction;
import persist.DBConnection;
import persist.DataMapper;
import persist.UnitOfWork;

public class DAOSettings implements DataMapper<Settings> {

	private WeakHashMap<UUID, Settings> map;

	private static DAOSettings inst = null;

	public static DAOSettings getInstance() {
		if (inst == null)
			inst = new DAOSettings();
		return inst;
	}

	private DAOSettings() {
		super();
		this.map = new WeakHashMap<UUID, Settings>();
	}

	@Override
	public Settings insert(Settings obj, Object... args) throws SQLException {
		String req = "INSERT INTO PROCOO_SETTINGS (id, maxPlayer, sizeX, sizeY, resourcesRound, resourcesProductByField, distanceMiniBetweenStartCities, id_game) VALUES (?,?,?,?,?,?,?,?)";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		UUID uuid = UUID.randomUUID();
		try {
			UUID gameId = (UUID) args[0];
			ps.setString(8, gameId.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			ps.setNull(8, java.sql.Types.VARCHAR);
		}
		ps.setString(1, uuid.toString());
		ps.setInt(2, obj.getMaxPlayer());
		ps.setInt(3, obj.getSizeX());
		ps.setInt(4, obj.getSizeY());
		ps.setInt(5, obj.getResourcesRound());
		ps.setInt(6, obj.getResourcesProductByField());
		ps.setInt(7, obj.getDistanceMiniBetweenStartCities());
		ps.executeUpdate();

		obj.setId(uuid);
		obj.addObserver(UnitOfWork.getInstance());

		map.put(uuid, obj);

		insertProbsLandsGeneration(obj, uuid);

		insertResourcesCostAction(obj, uuid);

		return obj;

	}

	@Override
	public void delete(UUID id) throws SQLException {
		String req = "DELETE PROCOO_SETTINGS WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		ps.setString(1, id.toString());
		ps.executeUpdate();

		if (map.containsKey(id) == true) {
			map.remove(id);
		}
	}

	@Override
	public void update(Settings obj, Object... args) throws SQLException {
		String req = "UPDATE PROCOO_SETTINGS SET maxPlayer = ?, sizeX = ?, sizeY = ?, resourcesRound = ?, resourcesProductByField = ?, distanceMiniBetweenStartCities = ?, id_game = ? WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps1 = c.prepareStatement(req);
		ps1.setInt(1, obj.getMaxPlayer());
		ps1.setInt(2, obj.getSizeX());
		ps1.setInt(3, obj.getSizeY());
		ps1.setInt(4, obj.getResourcesRound());
		ps1.setInt(5, obj.getResourcesProductByField());
		ps1.setInt(6, obj.getDistanceMiniBetweenStartCities());
		try {
			UUID gameId = (UUID) args[0];
			ps1.setString(7, gameId.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			ps1.setNull(7, java.sql.Types.VARCHAR);
		}
		ps1.setString(8, obj.getId().toString());
		ps1.executeUpdate();

		String req2 = "DELETE PROCOO_PROBSLANDGENERATION WHERE id_settings = ?";
		PreparedStatement ps2 = c.prepareStatement(req2);
		ps2.setString(1, obj.getId().toString());
		ps2.executeUpdate();

		insertProbsLandsGeneration(obj, obj.getId());

		String req3 = "DELETE PROCOO_RESOURCESCOSTACTION WHERE id_settings = ?";
		PreparedStatement ps3 = c.prepareStatement(req3);
		ps3.setString(1, obj.getId().toString());
		ps3.executeUpdate();

		insertResourcesCostAction(obj, obj.getId());

	}

	private void insertProbsLandsGeneration(Settings obj, UUID uuid) throws SQLException {
		String req = "INSERT INTO PROCOO_PROBSLANDGENERATION (id_settings, class, prob) VALUES (?,?,?)";
		Connection c = DBConnection.getConnection();
		HashMap<Class<? extends Land>, Float> selects = obj.getProbLandsGeneration();

		for (Map.Entry<Class<? extends Land>, Float> entry : selects.entrySet()) {
			Class<? extends Land> key = entry.getKey();
			Float value = entry.getValue();
			PreparedStatement ps = c.prepareStatement(req);
			ps.setString(1, uuid.toString());
			ps.setString(2, key.getName());
			ps.setFloat(3, value);
			ps.executeUpdate();
		}
	}

	private void insertResourcesCostAction(Settings obj, UUID uuid) throws SQLException {
		String req = "INSERT INTO PROCOO_RESOURCESCOSTACTION (id_settings, class, cost) VALUES (?,?,?)";
		Connection c = DBConnection.getConnection();
		HashMap<Class<? extends AbstractAction>, Integer> selects = obj.getResourcesCostAction();

		for (Map.Entry<Class<? extends AbstractAction>, Integer> entry : selects.entrySet()) {
			Class<? extends AbstractAction> key = entry.getKey();
			Integer value = entry.getValue();
			PreparedStatement ps = c.prepareStatement(req);
			ps.setString(1, uuid.toString());
			ps.setString(2, key.getName());
			ps.setInt(3, value);
			ps.executeUpdate();
		}
	}

	@Override
	public Settings findById(UUID id) throws SQLException {
		if (map.containsKey(id) == true) {
			return map.get(id);
		}
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, maxPlayer, sizeX, sizeY, resourcesRound, resourcesProductByField, distanceMiniBetweenStartCities, id_game FROM PROCOO_SETTINGS WHERE id = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		result.next();
		Integer maxPlayer = result.getInt(2);
		Integer sizeX = result.getInt(3);
		Integer sizeY = result.getInt(4);
		Integer resourcesRound = result.getInt(5);
		Integer resourcesProductByField = result.getInt(6);
		Integer distanceMiniBetweenStartCities = result.getInt(7);
		Settings settings = new Settings();
		settings.setId(id);
		settings.addObserver(UnitOfWork.getInstance());
		settings.setMaxPlayer(maxPlayer);
		settings.setSizeX(sizeX);
		settings.setSizeY(sizeY);
		settings.setResourcesRound(resourcesRound);
		settings.setResourcesProductByField(resourcesProductByField);
		settings.setDistanceMiniBetweenStartCities(distanceMiniBetweenStartCities);
		settings.setProbLandsGeneration(findProbLandsGenerationBySettingsId(id));
		settings.setResourcesCostAction(findResourcesCostActionBySettingsId(id));
		map.put(id, settings);
		return settings;
	}

	public HashMap<Class<? extends Land>, Float> findProbLandsGenerationBySettingsId(UUID id) throws SQLException {
		HashMap<Class<? extends Land>, Float> probLandsGeneration = new HashMap<>();
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id_settings, class, prob FROM PROCOO_PROBSLANDGENERATION WHERE id_settings = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		while (result.next()) {
			Float prob = result.getFloat(3);
			String className = result.getString(2);
			try {
				@SuppressWarnings("unchecked")
				Class<Land> oClass = (Class<Land>) Class.forName(className);
				probLandsGeneration.put(oClass, prob);
			} catch (ClassNotFoundException e) {
				throw new SQLException(e);
			}
		}
		return probLandsGeneration;
	}

	public HashMap<Class<? extends AbstractAction>, Integer> findResourcesCostActionBySettingsId(UUID id) throws SQLException {
		HashMap<Class<? extends AbstractAction>, Integer> resourcesCostAction = new HashMap<>();
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id_settings, class, cost FROM PROCOO_RESOURCESCOSTACTION WHERE id_settings = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		while (result.next()) {
			Integer cost = result.getInt(3);
			String className = result.getString(2);
			try {
				@SuppressWarnings("unchecked")
				Class<AbstractAction> oClass = (Class<AbstractAction>) Class.forName(className);
				resourcesCostAction.put(oClass, cost);
			} catch (ClassNotFoundException e) {
				throw new SQLException(e);
			}
		}
		return resourcesCostAction;
	}

	public Settings findByUniqueGameId(UUID id) throws SQLException {
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, maxPlayer, sizeX, sizeY, resourcesRound, resourcesProductByField, distanceMiniBetweenStartCities, id_game FROM PROCOO_SETTINGS WHERE id_game = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		result.next();
		UUID id_settings = UUID.fromString(result.getString(1));
		if (map.containsKey(id_settings) == true) {
			return map.get(id_settings);
		}
		Integer maxPlayer = result.getInt(2);
		Integer sizeX = result.getInt(3);
		Integer sizeY = result.getInt(4);
		Integer resourcesRound = result.getInt(5);
		Integer resourcesProductByField = result.getInt(6);
		Integer distanceMiniBetweenStartCities = result.getInt(7);
		Settings settings = new Settings();
		settings.setId(id);
		settings.addObserver(UnitOfWork.getInstance());
		settings.setMaxPlayer(maxPlayer);
		settings.setSizeX(sizeX);
		settings.setSizeY(sizeY);
		settings.setResourcesRound(resourcesRound);
		settings.setResourcesProductByField(resourcesProductByField);
		settings.setDistanceMiniBetweenStartCities(distanceMiniBetweenStartCities);
		settings.setProbLandsGeneration(findProbLandsGenerationBySettingsId(id_settings));
		settings.setResourcesCostAction(findResourcesCostActionBySettingsId(id_settings));
		map.put(id_settings, settings);
		return settings;
	}

}
