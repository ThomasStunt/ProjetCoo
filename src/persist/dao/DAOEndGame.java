package persist.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.WeakHashMap;

import model.game.EndGame;
import persist.DBConnection;
import persist.DataMapper;
import persist.UnitOfWork;
import persist.VirtualProxyBuilder;
import persist.factories.FactoryEndGamePlayers;

public class DAOEndGame implements DataMapper<EndGame> {

	private WeakHashMap<UUID, EndGame> map;

	private static DAOEndGame inst = null;

	public static DAOEndGame getInstance() {
		if (inst == null)
			inst = new DAOEndGame();
		return inst;
	}

	private DAOEndGame() {
		super();
		this.map = new WeakHashMap<UUID, EndGame>();
	}

	@Override
	public EndGame insert(EndGame obj, Object... args) throws SQLException {
		String req1 = "INSERT INTO PROCOO_ENDGAME (id, gameName, hostPlayer, nbRounds, winnerName) VALUES (?,?,?,?)";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps1 = c.prepareStatement(req1);
		UUID uuid = UUID.randomUUID();
		ps1.setString(1, uuid.toString());
		ps1.setString(2, obj.getGameName());
		ps1.setString(3, obj.getHostPlayer());
		ps1.setInt(4, obj.getNbRounds());
		ps1.setString(5, obj.getWinnerName());
		ps1.executeUpdate();

		insertAssoEndgamePlayers(obj, uuid);

		obj.setId(uuid);
		obj.addObserver(UnitOfWork.getInstance());

		map.put(uuid, obj);

		return obj;

	}

	@Override
	public void delete(UUID id) throws SQLException {
		String req = "DELETE PROCOO_ENDGAME WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		ps.setString(1, id.toString());
		ps.executeUpdate();

		if (map.containsKey(id) == true) {
			map.remove(id);
		}

	}

	@Override
	public void update(EndGame obj, Object... args) throws SQLException {
		String req1 = "UPDATE PROCOO_ENDGAME SET gameName = ?, hostPlayer = ?, nbRounds = ?, winnerName = ? WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps1 = c.prepareStatement(req1);
		ps1.setString(1, obj.getGameName());
		ps1.setString(2, obj.getHostPlayer());
		ps1.setInt(3, obj.getNbRounds());
		ps1.setString(4, obj.getWinnerName());
		ps1.setString(5, obj.getId().toString());
		ps1.executeUpdate();

		String req2 = "DELETE PROCOO_ENDGAME_ASSO_PLAYER WHERE id_endgame = ?";
		PreparedStatement ps2 = c.prepareStatement(req2);
		ps2.setString(1, obj.getId().toString());
		ps2.executeUpdate();

		insertAssoEndgamePlayers(obj, obj.getId());

	}

	private void insertAssoEndgamePlayers(EndGame obj, UUID uuid) throws SQLException {
		String req = "INSERT INTO PROCOO_ENDGAME_ASSO_PLAYER (id_endgame, player) VALUES (?,?)";
		Connection c = DBConnection.getConnection();
		for (String player : obj.getPlayers()) {
			PreparedStatement ps = c.prepareStatement(req);
			ps.setString(1, uuid.toString());
			ps.setString(2, player);
			ps.executeUpdate();
		}
	}

	@Override
	public EndGame findById(UUID id) throws SQLException {
		if (map.containsKey(id) == true) {
			return map.get(id);
		}
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, gameName, hostPlayer, nbRounds, winnerName FROM PROCOO_ENDGAME WHERE id = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		result.next();
		String gameName = result.getString(2);
		String hostPlayer = result.getString(3);
		Integer nbRounds = result.getInt(4);
		String winnerName = result.getString(5);
		EndGame eg = new EndGame(gameName, hostPlayer, null, nbRounds, winnerName);
		eg.setId(id);
		eg.addObserver(UnitOfWork.getInstance());
		eg.setPlayers(new VirtualProxyBuilder<List<String>>(List.class, new FactoryEndGamePlayers(id)).getProxy());
		map.put(id, eg);
		return eg;
	}

	public List<String> findPlayersByEndgameId(UUID id) throws SQLException {
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id_endgame, player FROM PROCOO_ENDGAME_ASSO_PLAYER WHERE id_endgame = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		List<String> players = new ArrayList<>();
		while (result.next()) {
			String player = result.getString(2);
			players.add(player);
		}

		String sql2 = "SELECT id, hostPlayer FROM PROCOO_ENDGAME WHERE id = ?";
		PreparedStatement stmt2 = c.prepareStatement(sql2);
		stmt2.setString(1, id.toString());
		ResultSet result2 = stmt2.executeQuery();
		result2.next();
		String host = result2.getString(2);
		players.add(host);

		return players;
	}

}
