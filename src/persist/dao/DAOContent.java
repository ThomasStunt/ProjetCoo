package persist.dao;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.WeakHashMap;

import model.player.content.Content;
import model.user.Player;
import persist.DBConnection;
import persist.DataMapper;
import persist.UnitOfWork;

public class DAOContent implements DataMapper<Content> {

	private WeakHashMap<UUID, Content> map;

	private static DAOContent inst = null;

	public static DAOContent getInstance() {
		if (inst == null)
			inst = new DAOContent();
		return inst;
	}

	private DAOContent() {
		super();
		this.map = new WeakHashMap<UUID, Content>();
	}

	@Override
	public Content insert(Content obj, Object... args) throws SQLException {
		String req = "INSERT INTO PROCOO_CONTENT (id, id_player, contentName, type, id_land) VALUES (?,?,?,?,?)";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		UUID uuid = UUID.randomUUID();
		try {
			UUID landId = (UUID) args[0];
			ps.setString(5, landId.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			ps.setNull(5, java.sql.Types.VARCHAR);
		}
		ps.setString(1, uuid.toString());
		ps.setString(2, obj.getOwner().getId().toString());
		ps.setString(3, obj.getContentName());
		ps.setString(4, obj.getType());

		ps.executeUpdate();

		obj.setId(uuid);
		obj.addObserver(UnitOfWork.getInstance());

		map.put(uuid, obj);

		return obj;
	}

	@Override
	public void delete(UUID id) throws SQLException {
		String req = "DELETE PROCOO_CONTENT WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps = c.prepareStatement(req);
		ps.setString(1, id.toString());
		ps.executeUpdate();

		if (map.containsKey(id) == true) {
			map.remove(id);
		}

	}

	@Override
	public void update(Content obj, Object... args) throws SQLException {
		String req = "UPDATE PROCOO_CONTENT SET id_player = ?, contentName = ?, type = ?, id_land = ? WHERE id = ?";
		Connection c = DBConnection.getConnection();
		PreparedStatement ps1 = c.prepareStatement(req);
		ps1.setString(1, obj.getOwner().getId().toString());
		ps1.setString(2, obj.getContentName());
		ps1.setString(3, obj.getType());
		try {
			UUID landId = (UUID) args[0];
			ps1.setString(4, landId.toString());
		} catch (ArrayIndexOutOfBoundsException e) {
			ps1.setNull(4, java.sql.Types.VARCHAR);
		}
		ps1.setString(5, obj.getId().toString());
		ps1.executeUpdate();
	}

	@Override
	public Content findById(UUID id) throws SQLException {
		if (map.containsKey(id) == true) {
			return map.get(id);
		}
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, id_player, contentName, type FROM PROCOO_CONTENT WHERE id = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		result.next();
		UUID idPlayer = UUID.fromString(result.getString(2));
		String contentName = result.getString(3);
		String type = result.getString(4);
		String contentClassName = type.substring(0, 1) + type.substring(1).toLowerCase();
		Object o = null;
		try {
			Class<?> contentClass = Class.forName("model.player.content." + contentClassName);
			Constructor<?> constr;
			constr = contentClass.getConstructor(Player.class, String.class);
			o = constr.newInstance(DAOPlayer.getInstance().findById(idPlayer), contentName);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException | ClassNotFoundException e) {
			throw new SQLException(e);
		}
		Content mContent = (Content) o;
		mContent.setId(id);
		mContent.addObserver(UnitOfWork.getInstance());
		map.put(id, mContent);
		return mContent;
	}

	public List<Content> findContentsByLandId(UUID id) throws SQLException {
		Connection c = DBConnection.getConnection();
		String sql = "SELECT id, id_player, contentName, type, id_land FROM PROCOO_CONTENT WHERE id_land = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, id.toString());
		ResultSet result = stmt.executeQuery();
		List<Content> contents = new ArrayList<>();
		while (result.next()) {
			UUID id_content = UUID.fromString(result.getString(1));
			Content content = findById(id_content);
			contents.add(content);
		}

		return contents;
	}

}
