package persist;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	/**
	 * The database {@link Connection}
	 */
	private static Connection connection;

	/**
	 * Get the {@link Connection} to the database used in this software.
	 *
	 * @return the {@link Connection} setted
	 * @throws SQLException
	 *             if a exception occured during the connection to the database
	 */
	public static Connection getConnection() throws SQLException {
		if (connection == null) {
			Connection connection = DriverManager.getConnection(
					"jdbc:oracle:thin:@oracle.fil.univ-lille1.fr:1521:filora", "PERRIER_ED", "saturnin");
			connection.setAutoCommit(true);
			return connection;
		} else {
			return connection;
		}
	}

}
