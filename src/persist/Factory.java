package persist;

/**
 * Generic interface Factory
 *
 * @param <T>
 *            type to return
 */
public interface Factory<T> {

	/**
	 * Create the ressource needed by calling the factory.
	 *
	 * @return new ressource of type T
	 */
	T create();
}
