package view.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.exceptions.ServiceException;
import model.game.EndGame;
import model.game.Game;
import model.game.Settings;
import model.lands.FieldLand;
import model.lands.Land;
import model.lands.MountainLand;
import model.lands.PlainLand;
import model.user.Player;
import model.user.player.actions.AbstractAction;
import model.user.player.actions.AttackArmyAction;
import model.user.player.actions.CreateArmyAction;
import model.user.player.actions.CreateCityAction;
import model.user.player.actions.MoveArmyAction;
import persist.DBConnection;
import persist.UnitOfWork;
import services.CreateNewGame;
import services.GetListEndGamePlayer;
import services.GetListFriendPlayer;
import services.GetListGamePlayer;
import services.GetListHostedGamePlayer;
import services.InitGameboardOfGame;
import services.RemoveFriendOfPlayer;
import view.utils.GenericListModel;
import view.utils.StringListModel;

public class HomeFrame extends JFrame {
	private static final long serialVersionUID = 1867404137201458652L;

	JPanel actualPanel;

	Player logged;

	private class HomePanel extends JPanel {
		private static final long serialVersionUID = 726411489044323494L;

		HomeFrame frame;

		JLabel nameLabel;
		JButton listPartiesButton;
		JButton histoPartiesButton;
		JButton createPartieButton;
		JButton listFriendsButton;
		JButton discButton;

		public HomePanel(HomeFrame frame) {
			this.frame = frame;

			this.setPreferredSize(new Dimension(400, 300));

			nameLabel = new JLabel("Bonjour "+logged.getName()); // Ici on peut rajouter le pseudo du type parce que why not
			listPartiesButton = new JButton("Liste de vos parties");
			listPartiesButton.setPreferredSize(new Dimension(200, 25));
			histoPartiesButton = new JButton("Historique");
			histoPartiesButton.setPreferredSize(new Dimension(200, 25));
			createPartieButton = new JButton("Créer une partie");
			createPartieButton.setPreferredSize(new Dimension(200, 25));
			listFriendsButton = new JButton("Liste des amis");
			listFriendsButton.setPreferredSize(new Dimension(200, 25));
			discButton = new JButton("Se déconnecter");
			discButton.setPreferredSize(new Dimension(200, 25));

			GridBagLayout gbl = new GridBagLayout();
			this.setLayout(gbl);

			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 1;
			gbc.insets = new Insets(20, 0, 0, 0);

			this.add(nameLabel, gbc);
			this.add(configListPartiesButton(), gbc);
			this.add(configHistoPartiesButton(), gbc);
			this.add(configCreatePartieButton(), gbc);
			this.add(configListFriendsButton(), gbc);
			this.add(configDiscButton(), gbc);
		}

		private JButton configListPartiesButton() {
			listPartiesButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						listPartiesButton.doClick();
				}
			});

			listPartiesButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					changePanel(new ListPartiesPanel(frame));
				}
			});
			return listPartiesButton;
		}

		private JButton configHistoPartiesButton() {
			histoPartiesButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					changePanel(new HistoPanel(frame));
				}
			});
			histoPartiesButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						histoPartiesButton.doClick();
				}
			});
			return histoPartiesButton;
		}

		private JButton configCreatePartieButton() {
			createPartieButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						createPartieButton.doClick();
				}
			});
			createPartieButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					changePanel(new CreatePartiePanel(frame));
				}
			});
			return createPartieButton;
		}

		private JButton configListFriendsButton() {
			listFriendsButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						listFriendsButton.doClick();
				}
			});
			listFriendsButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					changePanel(new ListFriendsPanel(frame));
				}
			});
			return listFriendsButton;
		}

		private JButton configDiscButton() {
			discButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						discButton.doClick();
				}
			});
			discButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					dispose();
					new ConnexionFrame();
				}
			});
			return discButton;
		}
	}

	private class ListPartiesPanel extends JPanel {
		private static final long serialVersionUID = 2489047316162395788L;

		HomeFrame frame;

		JList<Game> listParties;
		JButton joinPartie;
		JButton menuButton;
		JButton lookPartie;
		JLabel infoPartie;
		List<Game> parties;
		Game g;

		public ListPartiesPanel(HomeFrame frame) {
			listParties = new JList<Game>();
			listParties.setPreferredSize(new Dimension(150, 250));
			infoPartie = new JLabel();
			lookPartie = new JButton("Chercher une partie");
			lookPartie.setPreferredSize(new Dimension(115, 30));
			joinPartie = new JButton("Reprendre");
			joinPartie.setPreferredSize(new Dimension(115, 30));
			menuButton = new JButton("Retour");
			menuButton.setPreferredSize(new Dimension(115, 30));

			GridBagLayout gbl = new GridBagLayout();
			this.setLayout(gbl);

			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 1;
			gbc.gridy = 1;

			gbc.insets = new Insets(0, -200, 0, 0);
			this.add(configListParties(), gbc);
			gbc.insets = new Insets(-140, 180, 0, 0);
			this.add(configLookPartie(), gbc);
			gbc.insets = new Insets(-20, 180, 0, 0);
			this.add(infoPartie, gbc);
			gbc.insets = new Insets(40, 180, 0, 0);
			this.add(configJoinButton(), gbc);
			gbc.insets = new Insets(120, 180, 0, 0);
			this.add(configMenuButton(), gbc);
		}

		private JButton configLookPartie() {
			lookPartie.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new LookGameFrame(logged);
				}
			});
			lookPartie.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						lookPartie.doClick();
				}
			});
			return lookPartie;
		}

		private JList<Game> configListParties() {
			parties = new ArrayList<Game>();
			try {
				parties.addAll(GetListGamePlayer.getListGamePlayer(logged.getName()));
				parties.addAll(GetListHostedGamePlayer.getListHostedGamePlayer(logged.getName()));
			} catch (ServiceException e1) {
				e1.printStackTrace();
			}
			listParties.setModel(new GenericListModel<Game>(parties));
			listParties.addListSelectionListener(new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent e) {
					int selected = listParties.getSelectionModel().getMinSelectionIndex();
					infoPartie.setText("Nom de la partie : " + listParties.getModel().getElementAt(selected));
					g = listParties.getModel().getElementAt(selected);
				}
			});
			return listParties;
		}

		private JButton configMenuButton() {
			menuButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						menuButton.doClick();
				}
			});
			menuButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					changePanel(new HomePanel(frame));
				}
			});
			return menuButton;
		}

		private JButton configJoinButton() {
			joinPartie.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						joinPartie.doClick();
				}
			});
			joinPartie.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					new GameFrame(g, logged);
					dispose();
				}
			});
			return joinPartie;
		}
	}

	private class CreatePartiePanel extends JPanel {
		private static final long serialVersionUID = -1661242494461157766L;

		HomeFrame frame;

		JButton normalPartieButton;
		JButton persoPartieButton;
		JButton menuButton;

		public CreatePartiePanel(HomeFrame frame) {
			this.frame = frame;

			normalPartieButton = new JButton("Partie normale");
			normalPartieButton.setPreferredSize(new Dimension(185, 35));
			persoPartieButton = new JButton("Partie personnalisée");
			persoPartieButton.setPreferredSize(new Dimension(185, 35));
			menuButton = new JButton("Retour");
			menuButton.setPreferredSize(new Dimension(185, 35));

			GridBagLayout gbl = new GridBagLayout();
			this.setLayout(gbl);

			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 1;

			this.add(configNormalPartieButton(), gbc);
			gbc.insets = new Insets(20, 0, 0, 0);
			this.add(configPersoPartieButton(), gbc);
			gbc.insets = new Insets(100, 0, 0, 0);
			this.add(configMenuButton(), gbc);
		}

		private JButton configMenuButton() {
			menuButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					changePanel(new HomePanel(frame));
				}
			});
			menuButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						menuButton.doClick();
				}
			});
			return menuButton;
		}

		private JButton configNormalPartieButton() {
			normalPartieButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					changePanel(new PartieNormalPanel(frame));
				}
			});
			normalPartieButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						normalPartieButton.doClick();
				}
			});
			return normalPartieButton;
		}

		private JButton configPersoPartieButton() {
			persoPartieButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					changePanel(new PartiePersoPanel(frame));
				}
			});
			persoPartieButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						persoPartieButton.doClick();
				}
			});
			return persoPartieButton;
		}
	}

	private class PartieNormalPanel extends JPanel {
		private static final long serialVersionUID = -8431526840230920679L;

		HomeFrame frame;

		JLabel nameGame;
		JTextField nameGameField;
		JLabel maxPlayers;
		JTextField maxPlayersField;
		JLabel resourcesPerRound;
		JTextField resourcesPerRoundField;
		JButton createButton;
		JButton menuButton;

		public PartieNormalPanel(HomeFrame frame) {
			this.frame = frame;
			nameGame = new JLabel("Nom de la partie : ");
			maxPlayers = new JLabel("Nombre de joueurs max : ");
			resourcesPerRound = new JLabel("Ressources par tour : ");
			nameGameField = new JTextField();
			nameGameField.setPreferredSize(new Dimension(160, 20));
			maxPlayersField = new JTextField();
			maxPlayersField.setPreferredSize(new Dimension(25, 20));
			resourcesPerRoundField = new JTextField();
			resourcesPerRoundField.setPreferredSize(new Dimension(45, 20));

			createButton = new JButton("Créer");
			createButton.setPreferredSize(new Dimension(120, 25));
			menuButton = new JButton("Retour");
			menuButton.setPreferredSize(new Dimension(120, 25));

			GridBagLayout gbl = new GridBagLayout();
			this.setLayout(gbl);

			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(15, 0, 0, 0);

			gbc.gridx = 1;
			gbc.anchor = GridBagConstraints.LINE_END;
			this.add(nameGame, gbc);
			this.add(maxPlayers, gbc);
			this.add(resourcesPerRound, gbc);

			gbc.gridx = 2;
			gbc.anchor = GridBagConstraints.LINE_START;
			this.add(nameGameField, gbc);
			this.add(maxPlayersField, gbc);
			this.add(resourcesPerRoundField, gbc);

			gbc.gridx = 1;
			gbc.anchor = GridBagConstraints.CENTER;
			gbc.insets = new Insets(50, 0, 0, 0);
			this.add(configCreateButton(), gbc);
			gbc.gridx = 2;
			gbc.anchor = GridBagConstraints.CENTER;
			gbc.insets = new Insets(50, 0, 0, 0);
			this.add(configMenuButton(), gbc);
		}

		private JButton configMenuButton() {
			menuButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					changePanel(new CreatePartiePanel(frame));
				}
			});
			menuButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						menuButton.doClick();
				}
			});
			return menuButton;
		}

		private JButton configCreateButton() {
			createButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String name = nameGameField.getText();
					int maxPlayers = Integer.parseInt(maxPlayersField.getText());
					int resPerRound = Integer.parseInt(resourcesPerRoundField.getText());

					Settings s = new Settings();
					s.setMaxPlayer(maxPlayers);
					s.setResourcesRound(resPerRound);
					s.setSizeX(15);
					s.setSizeY(15);
					s.setResourcesProductByField(15);
					s.setDistanceMiniBetweenStartCities(40);
					HashMap< Class <? extends Land>, Float> coefLands = new HashMap<Class<? extends Land>, Float>();
					coefLands.put(PlainLand.class, 0.7f);
					coefLands.put(FieldLand.class, 0.15f);
					coefLands.put(MountainLand.class, 0.15f);
					s.setProbLandsGeneration(coefLands); 
					HashMap< Class <? extends AbstractAction>, Integer> costActions = new HashMap<Class<? extends AbstractAction>, Integer>();
					costActions.put(AttackArmyAction.class, 5);
					costActions.put(CreateArmyAction.class, 10);
					costActions.put(CreateCityAction.class, 20);
					costActions.put(MoveArmyAction.class, 1);
					s.setResourcesCostAction(costActions); 

					try {
						Game g = CreateNewGame.createNewGame(name, logged.getName(), s);

						try {
							InitGameboardOfGame.initGameboardOfGame(g);
						} catch (ServiceException e3) {
							e3.printStackTrace();
						}

						dispose();
						new GameFrame(g, logged);
					} catch (ServiceException e2) {
						e2.printStackTrace();
					}
				}
			});
			createButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						createButton.doClick();
				}
			});
			return createButton;
		}
	}

	private class PartiePersoPanel extends JPanel {
		private static final long serialVersionUID = -1445478957136141838L;

		HomeFrame frame;

		JLabel nameGame;
		JTextField nameGameField;
		JLabel maxPlayers;
		JTextField maxPlayersField;
		JLabel resourcesPerRound;
		JTextField resourcesPerRoundField;
		JLabel sizeX;
		JTextField sizeXField;
		JLabel sizeY;
		JTextField sizeYField;
		JLabel disBetCities;
		JTextField disBetCitiesField;
		JLabel resProdFields;
		JTextField resProdFieldsField;
		JButton createButton;
		JButton menuButton;

		public PartiePersoPanel(HomeFrame frame) {
			this.frame = frame;
			this.setPreferredSize(new Dimension(600, 300));

			nameGame = new JLabel("Nom de la partie : ");
			maxPlayers = new JLabel("Nombre de joueurs max : ");
			resourcesPerRound = new JLabel("Ressources par tour : ");
			sizeX = new JLabel("Taille carte X : ");
			sizeY = new JLabel("Taille carte Y : ");
			disBetCities = new JLabel("Distance entre villes : ");
			resProdFields = new JLabel("Ressources par champs : ");
			nameGameField = new JTextField();
			nameGameField.setPreferredSize(new Dimension(160, 20));
			maxPlayersField = new JTextField();
			maxPlayersField.setPreferredSize(new Dimension(25, 20));
			resourcesPerRoundField = new JTextField();
			resourcesPerRoundField.setPreferredSize(new Dimension(45, 20));
			sizeXField = new JTextField();
			sizeXField.setPreferredSize(new Dimension(25, 20));
			sizeYField = new JTextField();
			sizeYField.setPreferredSize(new Dimension(25, 20));
			disBetCitiesField = new JTextField();
			disBetCitiesField.setPreferredSize(new Dimension(25, 20));
			resProdFieldsField = new JTextField();
			resProdFieldsField.setPreferredSize(new Dimension(45, 20));

			createButton = new JButton("Créer");
			createButton.setPreferredSize(new Dimension(120, 25));
			menuButton = new JButton("Retour");
			menuButton.setPreferredSize(new Dimension(120, 25));

			GridBagLayout gbl = new GridBagLayout();
			this.setLayout(gbl);

			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 1;
			gbc.anchor = GridBagConstraints.LINE_END;
			gbc.insets = new Insets(5, 0, 0, 0);

			this.add(nameGame, gbc);
			this.add(maxPlayers, gbc);
			this.add(resourcesPerRound, gbc);
			this.add(sizeX, gbc);
			this.add(sizeY, gbc);
			this.add(disBetCities, gbc);
			this.add(resProdFields, gbc);

			gbc.gridx = 2;
			gbc.anchor = GridBagConstraints.LINE_START;
			this.add(nameGameField, gbc);
			this.add(maxPlayersField, gbc);
			this.add(resourcesPerRoundField, gbc);
			this.add(sizeXField, gbc);
			this.add(sizeYField, gbc);
			this.add(disBetCitiesField, gbc);
			this.add(resProdFieldsField, gbc);

			gbc.gridx = 1;
			gbc.anchor = GridBagConstraints.CENTER;
			gbc.insets = new Insets(25, 0, 0, 0);
			this.add(configCreateButton(), gbc);
			gbc.gridx = 2;
			gbc.anchor = GridBagConstraints.CENTER;
			gbc.insets = new Insets(25, 0, 0, 0);
			this.add(configMenuButton(), gbc);
		}

		private JButton configCreateButton() {
			createButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String name = nameGameField.getText();
					int maxPlayers = Integer.parseInt(maxPlayersField.getText());
					int resPerRound = Integer.parseInt(resourcesPerRoundField.getText());
					int sizeX = Integer.parseInt(sizeXField.getText());
					int sizeY = Integer.parseInt(sizeYField.getText());
					int disBetCi = Integer.parseInt("2");
					int resProdFie = Integer.parseInt("5");

					Settings s = new Settings();
					s.setMaxPlayer(maxPlayers);
					s.setResourcesRound(resPerRound);
					s.setSizeX(sizeX);
					s.setSizeY(sizeY);
					s.setResourcesProductByField(resProdFie);
					s.setDistanceMiniBetweenStartCities(disBetCi);
					HashMap<Class<? extends Land>, Float> probs = new HashMap<Class<? extends Land>, Float>();
					probs.put(PlainLand.class, (float) 0.8);
					probs.put(MountainLand.class, (float) 0.10);
					probs.put(FieldLand.class, (float) 0.10);
					s.setProbLandsGeneration(probs);
					HashMap< Class <? extends AbstractAction>, Integer> costActions = new HashMap<Class<? extends AbstractAction>, Integer>();
					costActions.put(AttackArmyAction.class, 5);
					costActions.put(CreateArmyAction.class, 10);
					costActions.put(CreateCityAction.class, 20);
					costActions.put(MoveArmyAction.class, 1);
					s.setResourcesCostAction(costActions);

					Game g;
					
					try {
						g = CreateNewGame.createNewGame(name, logged.getName(), s);
						
						try {
							InitGameboardOfGame.initGameboardOfGame(g);
						} catch (ServiceException e3) {
							e3.printStackTrace();
						}
						
						dispose();
						new GameFrame(g, logged);
					} catch (ServiceException e1) {
						e1.printStackTrace();
					}
				}
			});
			createButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						createButton.doClick();
				}
			});
			return createButton;
		}

		private JButton configMenuButton() {
			menuButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					changePanel(new CreatePartiePanel(frame));
				}
			});
			menuButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						menuButton.doClick();
				}
			});
			return menuButton;
		}
	}

	private class ListFriendsPanel extends JPanel {
		private static final long serialVersionUID = -5958488622447586680L;

		HomeFrame frame;

		JList<String> listFriends;
		JButton addFriend;
		JButton removeFriend;
		JButton menuButton;
		String namePerson = "";
		List<String> friends;

		public ListFriendsPanel(HomeFrame frame) {

			this.frame = frame;

			friends = new ArrayList<>();

			listFriends = new JList<String>();
			listFriends.setPreferredSize(new Dimension(150, 250));
			addFriend = new JButton("Ajouter");
			addFriend.setPreferredSize(new Dimension(115, 30));
			removeFriend = new JButton("Retirer");
			removeFriend.setPreferredSize(new Dimension(115, 30));
			menuButton = new JButton("Retour");
			menuButton.setPreferredSize(new Dimension(115, 30));

			GridBagLayout gbl = new GridBagLayout();
			this.setLayout(gbl);

			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 1;
			gbc.gridy = 1;

			gbc.insets = new Insets(0, -200, 0, 0);
			this.add(configListFriends(), gbc);
			gbc.insets = new Insets(-140, 180, 0, 0);
			this.add(configAddButton(), gbc);
			gbc.insets = new Insets(-60, 180, 0, 0);
			this.add(configDropButton(), gbc);
			gbc.insets = new Insets(120, 180, 0, 0);
			this.add(configMenuButton(), gbc);
		}

		private JList<String> configListFriends() {
			List<Player> loggedFriends;
			try {
				loggedFriends = GetListFriendPlayer.getListFriendPlayer(frame.getLogged().getName());
				for (Player player : loggedFriends) {
					friends.add(player.getName());
				}
			} catch (ServiceException e1) {
				JOptionPane.showMessageDialog(null, "An error occured while trying to get list friends.\n"
						+ e1.getMessage() + "\n" + e1.getCause().getMessage(), "Warning", JOptionPane.WARNING_MESSAGE);
			}
			listFriends.setModel(new StringListModel<String>(friends));
			listFriends.addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent e) {
					int selected = listFriends.getSelectionModel().getMinSelectionIndex();
					if (selected != -1)
						namePerson = listFriends.getModel().getElementAt(selected);
				}
			});
			return listFriends;
		}

		private JButton configAddButton() {
			addFriend.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					new AddFriendFrame(logged, listFriends, friends);
				}
			});
			addFriend.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						addFriend.doClick();
				}
			});
			return addFriend;
		}

		private JButton configDropButton() {
			removeFriend.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (namePerson != "") {
						try {
							RemoveFriendOfPlayer.removeFriendOfPlayer(logged.getName(), namePerson);
							friends.remove(namePerson);
							listFriends.setModel(new StringListModel<String>(friends));
							JOptionPane.showMessageDialog(null, "Friend " + namePerson + " remove.", "Information",
									JOptionPane.INFORMATION_MESSAGE);
						} catch (ServiceException e1) {
							JOptionPane.showMessageDialog(
									null, "An error occured while trying to remove a friends.\n" + e1.getMessage()
											+ "\n" + e1.getCause().getMessage(),
									"Warning", JOptionPane.WARNING_MESSAGE);
						}
					} else {
						JOptionPane.showMessageDialog(null, "Select a friend to remove.", "Information",
								JOptionPane.INFORMATION_MESSAGE);
					}
				}
			});
			removeFriend.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						removeFriend.doClick();
				}
			});
			return removeFriend;
		}

		private JButton configMenuButton() {
			menuButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					changePanel(new HomePanel(frame));
				}
			});
			menuButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						menuButton.doClick();
				}
			});
			return menuButton;
		}

	}

	private class HistoPanel extends JPanel {
		private static final long serialVersionUID = 6708598890334307433L;

		HomeFrame frame;

		JList<EndGame> oldGames;
		JLabel infos;
		JLabel gameInfos;
		JButton menuButton;

		List<EndGame> histos = new ArrayList<EndGame>();

		public HistoPanel(HomeFrame frame) {
			this.frame = frame;

			oldGames = new JList<EndGame>();
			oldGames.setPreferredSize(new Dimension(150, 250));
			infos = new JLabel("Infos");
			infos.setSize(50, 100);
			gameInfos = new JLabel();
			menuButton = new JButton("Retour");

			GridBagLayout gbl = new GridBagLayout();
			this.setLayout(gbl);

			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 1;
			gbc.gridy = 1;

			gbc.insets = new Insets(0, -200, 0, 0);
			this.add(configOldGames(), gbc);
			gbc.insets = new Insets(-140, 180, 0, 0);
			this.add(infos, gbc);
			gbc.insets = new Insets(-60, 180, 0, 0);
			this.add(gameInfos, gbc);
			gbc.insets = new Insets(160, 180, 0, 0);
			this.add(configMenuButton(), gbc);
		}

		private JList<EndGame> configOldGames() {
			try {
				histos = GetListEndGamePlayer.getListEndGamePlayer(logged.getName());
			} catch (ServiceException e1) {
				e1.printStackTrace();
			}
			oldGames.setModel(new GenericListModel<EndGame>(histos));
			oldGames.addListSelectionListener(new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent e) {
					int selected = oldGames.getSelectionModel().getMinSelectionIndex();
					System.out.println(oldGames.getModel().getElementAt(selected).getDescription());
					infos.setText("<html>Infos - Partie<br/>"
							+ oldGames.getModel().getElementAt(selected).getDescriptionHTML() + "</html>");
				}
			});
			return oldGames;
		}

		private JButton configMenuButton() {
			menuButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					changePanel(new HomePanel(frame));
				}
			});
			menuButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						menuButton.doClick();
				}
			});
			return menuButton;
		}
	}

	public Player getLogged() {
		return logged;
	}

	public void setLogged(Player logged) {
		this.logged = logged;
	}

	@Override
	public void dispose() {
		UnitOfWork.getInstance().commit();
		super.dispose();
	}

	public HomeFrame(Player logged) {
		super("Cyvylyzassion - Menu");
		this.logged = logged;
		actualPanel = new HomePanel(this);
		this.add(actualPanel);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				UnitOfWork.getInstance().commit();
				try {
					DBConnection.getConnection().close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void changePanel(JPanel panel) {
		this.setVisible(false);
		this.remove(actualPanel);
		actualPanel = panel;
		this.setPreferredSize(panel.getPreferredSize());
		this.add(actualPanel);
		this.setVisible(true);
	}
}