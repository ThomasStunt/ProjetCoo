package view.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import model.exceptions.ServiceException;
import model.game.Game;
import model.player.content.Content;
import model.user.Player;
import services.CheckEndGame;
import services.NextTurn;
import services.StartGame;

public class GameFrame extends JFrame {
	private static final long serialVersionUID = -5497305867904749828L;
	
	Player logged;

	private class GamePanel extends JPanel {
		private static final long serialVersionUID = 1235934289631650677L;

		public GamePanel(Game g, Player p) {
			GridBagLayout gbl = new GridBagLayout();
			this.setLayout(gbl);

			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridy = 1;
			this.add(new TopGamePanel(g), gbc);

			gbc.gridy = 2;
			this.add(new BottomGamePanel(g), gbc);
		}

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Image b;
			try {
				b = ImageIO.read(new File("lebackground.jpg"));
				g.drawImage(b, 0, 0, null);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private class TopGamePanel extends JPanel {
		private static final long serialVersionUID = -7074320389147646558L;

		Integer sizeX;
		Integer sizeY;

		public TopGamePanel(Game g) {
			try {
				StartGame.startGame(g, logged);
			} catch (ServiceException e1) {
				e1.printStackTrace();
			}
			
			sizeX = g.getSettings().getSizeX();
			sizeY = g.getSettings().getSizeY();
			this.setPreferredSize(new Dimension(sizeX * 40, sizeY * 40));

			GridLayout gl = new GridLayout(sizeX, sizeY);
			this.setLayout(gl);

			for (int i = 0; i < sizeX; i++) {
				for (int j = 0; j < sizeY; j++) {
					GameCell gc = new GameCell(g, i, j);
					this.add(gc, gl);
				}
			}
		}

		private class GameCell extends JPanel {
			private static final long serialVersionUID = 5596321152887039349L;

			int coordX;
			int coordY;
			Game ga;
			List<Content> listContent;

			public GameCell(Game g, int x, int y) {
				this.ga = g;
				this.coordX = x;
				this.coordY = y;
				listContent = g.getGameBoard().getMap()[coordX][coordY].getContents();

				try {
				} catch (Exception e) {
					System.out.println(e);
				}

				this.setBorder(new LineBorder(Color.BLACK));

				this.addMouseListener(new MouseAdapter() {
					public void mousePressed(MouseEvent e) {
						System.out.println(listContent);
					}
				});
			}

			@Override
			public String toString() {
				return "game cell " + coordX + ", " + coordY;
			}

			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				Image bg;
				Image moutain;
				Image field;
				try {
					bg = ImageIO.read(new File("plaine.png"));
					moutain = ImageIO.read(new File("moutain.png"));
					field = ImageIO.read(new File("champ.png"));
					if (ga.getGameBoard().getMap()[coordX][coordY].getType() == "PLAIN") {
						g.drawImage(bg, 0, 0, null);
					} else if (ga.getGameBoard().getMap()[coordX][coordY].getType() == "MOUNTAIN") {
						g.drawImage(moutain, 0, 0, null);
					} else {
						g.drawImage(field, 0, 0, null);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}

	private class BottomGamePanel extends JPanel {
		private static final long serialVersionUID = -2947158142985395091L;

		Game g;
		JButton menuButton;
		JButton validateTurnButton;

		public BottomGamePanel(Game g) {
			menuButton = new JButton("Retour");
			validateTurnButton = new JButton("Valider son tour");
			this.add(configMenuButton());
			this.add(configValidateButton());
			this.setOpaque(false);
		}

		private JButton configMenuButton() {
			menuButton.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						menuButton.doClick();
				}
			});
			menuButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
					new HomeFrame(logged);
				}
			});
			return menuButton;
		}
		
		private JButton configValidateButton() {
			validateTurnButton.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if(e.getKeyCode() == KeyEvent.VK_ENTER) {
						validateTurnButton.doClick();
					}
				}
			});
			validateTurnButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					NextTurn.nextTurn(g);
					try {
						if(CheckEndGame.checkEndGame(g)) {
							dispose();
						}
					} catch (ServiceException e1) {
						e1.printStackTrace();
					}
				}
			});
			return validateTurnButton;
		}
	}

	public GameFrame(Game g, Player p) {
		super("Cyvylyzassion - Partie en cours");
		this.logged = p;
		this.add(new GamePanel(g, logged));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}

	@Override
	public void dispose() {
		super.dispose();
	}
}
