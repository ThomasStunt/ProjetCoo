package view.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import model.exceptions.ServiceException;
import model.user.Player;
import services.LogIn;

public class ConnexionFrame extends JFrame {
	private static final long serialVersionUID = -1292805357515964628L;

	private class ConnexionPanel extends JPanel {
		private static final long serialVersionUID = -8989710771836030123L;

		JLabel loginLabel;
		JLabel passwdLabel;
		JTextField loginTF;
		JTextField passwdTF;
		JButton logButton;
		JButton createPlayerButton;

		public ConnexionPanel() {
			this.setPreferredSize(new Dimension(300, 200));

			loginLabel = new JLabel("Login :");
			passwdLabel = new JLabel("Password :");
			loginTF = new JTextField();
			loginTF.setPreferredSize(new Dimension(120, 20));
			passwdTF = new JPasswordField();
			passwdTF.setPreferredSize(new Dimension(120, 20));
			logButton = new JButton("Log in");
			createPlayerButton = new JButton("Create Player");

			GridBagLayout gbl = new GridBagLayout();
			this.setLayout(gbl);

			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 0;

			this.add(loginLabel, gbc);
			gbc.insets = new Insets(4, 0, 0, 0);
			this.add(loginTF, gbc);
			gbc.insets = new Insets(20, 0, 0, 0);
			this.add(passwdLabel, gbc);
			gbc.insets = new Insets(4, 0, 0, 0);
			this.add(passwdTF, gbc);

			gbc.insets = new Insets(25, 0, 0, 0);
			this.add(configLogButton(), gbc);

			gbc.insets = new Insets(20, 0, 0, 0);
			this.add(configCreatePlayerButton(), gbc);
		}

		private JButton configCreatePlayerButton() {
			createPlayerButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					// dispose();
					new CreatePlayerFrame();
				}
			});

			createPlayerButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						createPlayerButton.doClick();
				}
			});

			return createPlayerButton;
		}

		private JButton configLogButton() {
			logButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (loginTF.getText().isEmpty()) {
						new JOptionPane();
						JOptionPane.showMessageDialog(null, "Empty name.", "Warning", JOptionPane.WARNING_MESSAGE);
					} else {
						try {
							Player logged = LogIn.login(loginTF.getText());
							dispose();
							new HomeFrame(logged);
						} catch (ServiceException e1) {
							new JOptionPane();
							JOptionPane.showMessageDialog(null,
									"Player unreachable.\n" + e1.getMessage() + "\n" + e1.getCause().getMessage(),
									"Warning", JOptionPane.WARNING_MESSAGE);
						}
					}

				}
			});

			logButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						logButton.doClick();
				}
			});
			return logButton;
		}
	}

	public ConnexionFrame() {
		super("Cyvylyzassion - Connexion");
		ConnexionPanel connPan = new ConnexionPanel();
		this.add(connPan);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}

	@Override
	public void dispose() {
		super.dispose();
	}
}
