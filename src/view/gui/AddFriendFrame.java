package view.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.exceptions.ServiceException;
import model.user.Player;
import services.AddFriendToPlayer;
import view.utils.StringListModel;

public class AddFriendFrame extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = -5953659989567796120L;

	Player logged;

	List<String> friends;

	JList<String> listFriends;

	public AddFriendFrame(Player logged, JList<String> listFriends, List<String> friends) {
		super("Add friend");
		this.friends = friends;
		this.listFriends = listFriends;
		this.logged = logged;
		AddFriendPanel connPan = new AddFriendPanel(this);
		this.add(connPan);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setResizable(false);
		this.pack();
		this.setVisible(true);
	}

	private class AddFriendPanel extends JPanel {

		/**
		 *
		 */
		private static final long serialVersionUID = -1188168315089389385L;

		AddFriendFrame frame;

		JLabel playerNameLabel;
		JTextField fieldName;
		JButton okButton;

		public AddFriendPanel(AddFriendFrame frame) {
			this.frame = frame;
			this.setPreferredSize(new Dimension(300, 50));
			playerNameLabel = new JLabel("Name :");
			fieldName = new JTextField();
			fieldName.setPreferredSize(new Dimension(120, 20));
			okButton = new JButton("Ok");

			GridBagLayout gbl = new GridBagLayout();
			this.setLayout(gbl);

			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 0;

			this.add(playerNameLabel, gbc);
			gbc.gridx = 1;
			this.add(fieldName, gbc);
			gbc.gridx = 2;
			this.add(configButton(), gbc);
		}

		private JButton configButton() {
			okButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (fieldName.getText().isEmpty()) {
						new JOptionPane();
						JOptionPane.showMessageDialog(null, "Empty name.", "Warning", JOptionPane.WARNING_MESSAGE);
					}
					try {
						AddFriendToPlayer.addFriendToPlayer(frame.getLogged().getName(), fieldName.getText());
						new JOptionPane();
						JOptionPane.showMessageDialog(null, "Friend " + fieldName.getText() + " added.", "Information",
								JOptionPane.INFORMATION_MESSAGE);
						friends.add(fieldName.getText());
						listFriends.setModel(new StringListModel<String>(friends));
						frame.dispose();
					} catch (ServiceException e1) {
						new JOptionPane();
						JOptionPane
								.showMessageDialog(
										null, "An error occured while trying to save a new friend.\n" + e1.getMessage()
												+ "\n" + e1.getCause().getMessage(),
										"Warning", JOptionPane.WARNING_MESSAGE);

					}
				}
			});

			okButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						okButton.doClick();
				}
			});

			return okButton;
		}

	}

	public Player getLogged() {
		return logged;
	}

	public void setLogged(Player logged) {
		this.logged = logged;
	}

}
