package view.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.exceptions.ServiceException;
import model.game.Game;
import model.user.Player;
import persist.UnitOfWork;
import services.GetListHostedGamePlayer;

public class LookGameFrame extends JFrame {
	private static final long serialVersionUID = 6607694700927103313L;
	
	Player logged;

	private class LookGamePanel extends JPanel {
		private static final long serialVersionUID = -4769173880950012543L;
		
		JLabel gameName;
		JTextField jtf;
		JButton searchButton;
		JButton menuButton;
		
		public LookGamePanel() {
			searchButton = new JButton("Chercher");
			menuButton = new JButton("Menu");
			gameName = new JLabel("Nom du joueur : ");
			jtf = new JTextField();
			jtf.setPreferredSize(new Dimension(120, 25));

			GridBagLayout gbl = new GridBagLayout();
			this.setLayout(gbl);
			
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(20, 10, 0, 0);
			
			gbc.gridx = 1;
			gbc.anchor = GridBagConstraints.LINE_END;
			this.add(gameName, gbc);
			gbc.anchor = GridBagConstraints.CENTER;
			this.add(configSearchButton(), gbc);
			
			gbc.gridx = 2;
			gbc.anchor = GridBagConstraints.LINE_START;
			this.add(jtf, gbc);
			gbc.anchor = GridBagConstraints.CENTER;
			this.add(configMenuButton(), gbc);
		}
		
		private JButton configSearchButton() {
			searchButton.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if(e.getKeyCode() == KeyEvent.VK_ENTER)
						searchButton.doClick();
				}
			});
			searchButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						List<Game> listG = GetListHostedGamePlayer.getListHostedGamePlayer(jtf.getText());
						for(Game g : listG) {
							if(JOptionPane.showConfirmDialog(null, "Voulez vous rejoindre la partie "+g.getName()+" ?") == JOptionPane.YES_OPTION) {
								System.out.println("çamarche");
							} else {
								System.out.println("onarrete");
							}
						}
					} catch (ServiceException e1) {
						e1.printStackTrace();
					}
				}
			});
			return searchButton;
		}
		
		private JButton configMenuButton() {
			menuButton.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER)
						menuButton.doClick();
				}
			});
			menuButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			return menuButton;
		}
	}
	
	public LookGameFrame(Player logged) {
		super("Cyvylyzassion - Recherche");
		this.logged = logged;
		this.setPreferredSize(new Dimension(300, 150));
		this.add(new LookGamePanel());
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}
	
	public void dispose() {
		UnitOfWork.getInstance().commit();
		super.dispose();
	}
}
