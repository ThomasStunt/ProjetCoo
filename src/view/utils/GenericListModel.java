package view.utils;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

public class GenericListModel<T> implements ListModel<T> {

	public List<T> list;

	public GenericListModel() {
		list = new ArrayList<T>();
	}

	public GenericListModel(List<T> li) {
		list = new ArrayList<T>(li);
	}

	@Override
	public int getSize() {
		return list.size();
	}

	@Override
	public T getElementAt(int index) {
		return list.get(index);
	}

	public List<T> addList(List<T> li) {
		for(T t : li) {
			list.add(t);
		}
		return list;
	}
	
	@Override
	public void addListDataListener(ListDataListener l) {
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
	}

	public void remove(T s) {
		if (list.contains(s))
			list.remove(s);
	}
}
