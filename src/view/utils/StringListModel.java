package view.utils;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

@SuppressWarnings("hiding")
public class StringListModel<String> implements ListModel<String> {

	public List<String> list;
	
	public StringListModel() {
		list = new ArrayList<String>();
	}
	
	public StringListModel(List<String> li) {
		list = new ArrayList<String>(li);
	}
	
	public int getSize() {
		return list.size();
	}

	public String getElementAt(int index) {
		return list.get(index);
	}

	public void addListDataListener(ListDataListener l) {
	}

	public void removeListDataListener(ListDataListener l) {
	}
	
	public void remove(String s) {
		if(list.contains(s))
			list.remove(s);
	}

}
