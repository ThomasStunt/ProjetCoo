package services;

import model.exceptions.ActionException;
import model.exceptions.ServiceException;
import model.game.Game;
import model.lands.Land;
import model.user.Player;
import model.user.player.actions.CreateCityAction;

public class ActionCreateCity {

	public static void actionCreateCity(Game game, Player player, Land land, String cityName) throws ServiceException {
		Integer cost = game.getSettings().getResourcesCostAction().get(CreateCityAction.class);
		if (cost == null) {
			cost = 1;
		}
		CreateCityAction act = new CreateCityAction(cost, player, land, cityName);
		try {
			act.doAction();
		} catch (ActionException e) {
			throw new ServiceException("Can't create a city here.", e);
		}
	}

}
