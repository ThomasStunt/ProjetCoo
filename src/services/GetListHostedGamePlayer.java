package services;

import java.sql.SQLException;
import java.util.List;

import model.exceptions.ServiceException;
import model.game.Game;
import persist.dao.DAOPlayer;

public class GetListHostedGamePlayer {

	public static List<Game> getListHostedGamePlayer(String player) throws ServiceException {
		try {
			return DAOPlayer.getInstance().findByUniqueName(player).getHostedGames();
		} catch (SQLException e) {
			throw new ServiceException("Unable to reach Games List of Player " + player + ".", e);
		}
	}

}
