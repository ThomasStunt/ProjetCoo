package services;

import java.sql.SQLException;

import model.exceptions.ServiceException;
import model.game.EndGame;
import model.user.Player;
import persist.dao.DAOEndGame;
import persist.dao.DAOPlayer;

public class AddEndGameToPlayer {

	public static void addEndGameToPlayer(String player, EndGame endgame) throws ServiceException {
		Player p;

		try {
			if (endgame.getId() == null) {
				DAOEndGame.getInstance().insert(endgame);
			}
		} catch (SQLException e1) {
			throw new ServiceException("Unable to insert EndGame.", e1);
		}

		try {
			p = DAOPlayer.getInstance().findByUniqueName(player);
		} catch (SQLException e) {
			throw new ServiceException("Unable to reach Player " + player + ".", e);
		}
		p.addEndGames(endgame);
	}

}
