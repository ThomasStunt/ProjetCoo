package services;

import java.sql.SQLException;

import model.exceptions.ServiceException;
import model.game.Game;
import model.user.Player;
import persist.dao.DAOPlayer;

public class RemoveGameOfHostPlayer {

	public static void removeGameOfHostPlayer(String host, Game game) throws ServiceException {
		Player pHost;
		try {
			pHost = DAOPlayer.getInstance().findByUniqueName(host);
			pHost.removeHostedGame(game);
		} catch (SQLException e) {
			throw new ServiceException("Unable to reach Player " + host + ".", e);
		}

	}

}
