package services;

import java.sql.SQLException;

import model.exceptions.ServiceException;
import model.game.Game;
import model.game.Settings;
import model.game.state.WaitingState;
import model.user.Player;
import persist.dao.DAOGame;
import persist.dao.DAOPlayer;

public class CreateNewGame {

	public static Game createNewGame(String name, String host, Settings settings) throws ServiceException {
		Player pHost;
		try {
			pHost = DAOPlayer.getInstance().findByUniqueName(host);
		} catch (SQLException e1) {
			throw new ServiceException("Unable to reach Player  " + host + ".", e1);
		}
		Game newGame = new Game(name, pHost, settings);
		newGame.setState(new WaitingState());
		try {
			DAOGame.getInstance().insert(newGame);
		} catch (SQLException e) {
			throw new ServiceException("Unable to create a new Game.", e);
		}
		pHost.addHostedGame(newGame);
		return newGame;
	}

}
