package services;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;

import model.exceptions.ServiceException;
import model.game.Game;
import model.game.GameBoard;
import model.lands.Land;
import persist.dao.DAOGameBoard;

public class InitGameboardOfGame {

	public static void initGameboardOfGame(Game game) throws ServiceException {
		HashMap<Class<? extends Land>, Float> probLands = game.getSettings().getProbLandsGeneration();
		Float countTot = 0f;
		for (Entry<Class<? extends Land>, Float> entry : probLands.entrySet()) {
			Float value = entry.getValue();
			countTot += value;
		}
		if (countTot != 1f) {
			throw new ServiceException("Total prob for lands not equals to 1.");
		}
		Land[][] land = new Land[game.getSettings().getSizeX()][game.getSettings().getSizeY()];
		for (int i = 0; i < game.getSettings().getSizeX(); i++) {
			for (int j = 0; j < game.getSettings().getSizeX(); j++) {
				Float count = 0f;
				Random rand = new Random();
				Float randFloat = rand.nextFloat();
				for (Entry<Class<? extends Land>, Float> entry : probLands.entrySet()) {
					Class<? extends Land> cLand = entry.getKey();
					Float value = count + entry.getValue();
					if (randFloat >= count && randFloat <= value) {
						try {
							land[i][j] = (Land) Class.forName(cLand.getName()).getConstructor(int.class, int.class)
									.newInstance(i, j);
							land[i][j].setContents(new ArrayList<>());
						} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
								| InvocationTargetException | NoSuchMethodException | SecurityException
								| ClassNotFoundException e) {
							throw new ServiceException("Unable to init gameboard's lands.", e);
						}
						break;
					}
					count = value;
				}
			}
		}
		GameBoard gb = new GameBoard(game.getSettings());
		gb.setMap(land);
		try {
			DAOGameBoard.getInstance().insert(gb, game.getId());
		} catch (SQLException e) {
			throw new ServiceException("Unable to insert new Gameboard fo Game " + game.getName() + ".", e);
		}
		game.setGameBoard(gb);
	}

}
