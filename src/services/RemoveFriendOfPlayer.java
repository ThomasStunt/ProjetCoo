package services;

import java.sql.SQLException;

import model.exceptions.ServiceException;
import model.user.Player;
import persist.dao.DAOPlayer;

public class RemoveFriendOfPlayer {

	public static void removeFriendOfPlayer(String player, String friend) throws ServiceException {
		Player nFriend;
		try {
			nFriend = DAOPlayer.getInstance().findByUniqueName(friend);
		} catch (SQLException e) {
			throw new ServiceException("Unable to reach Player " + friend + ".", e);
		}
		try {
			DAOPlayer.getInstance().findByUniqueName(player).removeFriend(nFriend);
		} catch (SQLException e) {
			throw new ServiceException("Unable to remove Player " + friend + "of friends of " + player + ".", e);
		}
	}

}
