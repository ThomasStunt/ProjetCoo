package services;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import model.game.Game;
import model.lands.Land;
import model.player.content.City;
import model.user.Player;

public class InitStartCitiesOfGame {

	public static void initStartCitiesOfGame(Game game) {
		Integer x = game.getGameBoard().getSizeX();
		Integer y = game.getGameBoard().getSizeY();
		Random rand = new Random();
		Integer hostX = rand.nextInt(x);
		Integer hostY = rand.nextInt(y);
		City cityHost = new City(game.getHost(), "Start " + game.getHost().getName());
		game.getGameBoard().getMap()[hostX][hostY].addContent(cityHost);
		List<Land> landsCities = new ArrayList<>();
		landsCities.add(game.getGameBoard().getMap()[hostX][hostY]);
		List<Player> players = game.getPlayers();
		Integer distMin = game.getSettings().getDistanceMiniBetweenStartCities();
		for (Player player : players) {
			boolean flag1 = false;
			while (flag1) {
				boolean flag2 = true;
				Integer tmpX = rand.nextInt(x);
				Integer tmpY = rand.nextInt(y);
				for (Land land : landsCities) {
					Integer tmpDist = Math.abs(tmpX - land.getX()) + Math.abs(tmpY - land.getY()) + 1;
					if (tmpDist < distMin) {
						flag2 = false;
					}
				}
				if (flag2) {
					flag1 = true;
					City city = new City(player, "Start " + player.getName());
					game.getGameBoard().getMap()[tmpX][tmpY].addContent(city);
					landsCities.add(game.getGameBoard().getMap()[tmpX][tmpY]);
				}
			}
		}
	}

}
