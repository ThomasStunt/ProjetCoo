package services;

import model.game.Game;
import model.lands.Land;
import model.player.content.City;
import model.player.content.Content;
import model.user.Player;

public class SetResourcesToPlayer {

	public static void setResourcesToPlayer(Game game, Player player) {
		Integer fields = 0;
		Land[][] map = game.getGameBoard().getMap();
		for (int i = 0; i < game.getGameBoard().getSizeX(); i++) {
			for (int j = 0; j < game.getGameBoard().getSizeY(); j++) {
				for (Content content : map[i][j].getContents()) {
					if (content instanceof City && content.getOwner().equals(player)) {
						fields++;
					}
				}
			}
		}
		player.setRessources(
				game.getSettings().getResourcesRound() + (game.getSettings().getResourcesProductByField() * fields));
	}

}
