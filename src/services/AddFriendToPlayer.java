package services;

import java.sql.SQLException;

import model.exceptions.ServiceException;
import model.user.Player;
import persist.dao.DAOPlayer;

public class AddFriendToPlayer {

	public static void addFriendToPlayer(String player, String friend) throws ServiceException {
		Player nFriend;
		try {
			nFriend = DAOPlayer.getInstance().findByUniqueName(friend);
		} catch (SQLException e) {
			throw new ServiceException("Unable to reach Player " + friend + ".", e);
		}
		try {
			DAOPlayer.getInstance().findByUniqueName(player).addFriend(nFriend);
		} catch (SQLException e) {
			throw new ServiceException("Unable to add Player " + friend + "to friends of " + player + ".", e);
		}

	}

}
