package services;

import java.sql.SQLException;
import java.util.List;

import model.exceptions.ServiceException;
import model.user.Player;
import persist.dao.DAOPlayer;

public class GetListFriendPlayer {

	public static List<Player> getListFriendPlayer(String player) throws ServiceException {
		try {
			return DAOPlayer.getInstance().findByUniqueName(player).getFriends();
		} catch (SQLException e) {
			throw new ServiceException("Unable to reach friends of Player " + player + ".", e);
		}
	}

}
