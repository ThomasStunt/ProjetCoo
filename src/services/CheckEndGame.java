package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.exceptions.ServiceException;
import model.game.EndGame;
import model.game.Game;
import model.lands.Land;
import model.player.content.City;
import model.player.content.Content;
import model.user.Player;
import persist.dao.DAOGame;

public class CheckEndGame {

	public static Boolean checkEndGame(Game game) throws ServiceException {
		HashMap<Player, Integer> nbCityByPlayer = new HashMap<>();
		List<Player> players = game.getPlayers();
		nbCityByPlayer.put(game.getHost(), 0);
		for (Player player : players) {
			nbCityByPlayer.put(player, 0);
		}
		Land[][] map = game.getGameBoard().getMap();
		Integer x = game.getGameBoard().getSizeX();
		Integer y = game.getGameBoard().getSizeY();
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				Land tmp = map[i][j];
				List<Content> contents = tmp.getContents();
				for (Content content : contents) {
					if (content instanceof City) {
						City city = (City) content;
						Integer nbCity = nbCityByPlayer.get(city.getOwner());
						nbCityByPlayer.put(city.getOwner(), nbCity + 1);
					}
				}
			}
		}
		Player winner = null;

		for (Map.Entry<Player, Integer> entry : nbCityByPlayer.entrySet()) {
			Player key = entry.getKey();
			Integer value = entry.getValue();
			if (value > 0 && winner == null) {
				winner = key;
			} else if (value > 0 && winner != null) {
				return false;
			}
		}

		List<String> playersGame = new ArrayList<>();
		for (Player player : game.getPlayers()) {
			playersGame.add(player.getName());
		}
		EndGame endGame = new EndGame(game.getName(), game.getHost().getName(), playersGame,
				game.getRound().getNumberRound(), winner.getName());
		AddEndGameToPlayer.addEndGameToPlayer(game.getHost().getName(), endGame);
		for (Player player : game.getPlayers()) {
			AddEndGameToPlayer.addEndGameToPlayer(player.getName(), endGame);
		}
		try {
			DAOGame.getInstance().delete(game.getId());
		} catch (SQLException e) {
			throw new ServiceException("Unable to delete the finished game, but the EndGame object still create.", e);
		}
		return true;
	}

}
