package services;

import model.game.Game;
import model.user.Player;
import persist.UnitOfWork;

public class NextTurn {

	public static Player nextTurn(Game game) {
		if (!game.getRound().getPlayersOrder().hasNext()) {
			game.getRound().getPlayersOrder().reset();
			game.getRound().setNumberRound(game.getRound().getNumberRound() + 1);
		}
		Player nextPlayer = game.getRound().getPlayersOrder().next();
		SetResourcesToPlayer.setResourcesToPlayer(game, nextPlayer);
		UnitOfWork.getInstance().commit();
		return nextPlayer;
	}

}
