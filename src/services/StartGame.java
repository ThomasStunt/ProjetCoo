package services;

import java.util.ArrayList;
import java.util.List;

import model.exceptions.ServiceException;
import model.game.Game;
import model.game.round.IteratListPlayer;
import model.game.round.Round;
import model.game.state.RunningState;
import model.user.Player;

public class StartGame {

	public static Player startGame(Game game, Player execPlayer) throws ServiceException {
		if (!game.getHost().equals(execPlayer)) {
			throw new ServiceException("Only the host Player can begin game.");
		}
		InitGameboardOfGame.initGameboardOfGame(game);
		InitStartCitiesOfGame.initStartCitiesOfGame(game);
		List<Player> players = new ArrayList<>();
		players.add(game.getHost());
		players.addAll(game.getPlayers());
		Round round = new Round(0, new IteratListPlayer(players));
		game.setRound(round);
		game.setState(new RunningState());
		return NextTurn.nextTurn(game);
	}

}
