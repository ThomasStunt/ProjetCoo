package services;

import model.exceptions.ActionException;
import model.exceptions.ServiceException;
import model.game.Game;
import model.lands.Land;
import model.user.Player;
import model.user.player.actions.CreateArmyAction;

public class ActionCreateArmy {

	public static void actionCreateArmy(Game game, Player player, Land land, String armyName) throws ServiceException {
		Integer cost = game.getSettings().getResourcesCostAction().get(CreateArmyAction.class);
		if (cost == null) {
			cost = 1;
		}
		CreateArmyAction act = new CreateArmyAction(cost, player, land, armyName);
		try {
			act.doAction();
		} catch (ActionException e) {
			throw new ServiceException("Can't create an army.", e);
		}
	}

}
