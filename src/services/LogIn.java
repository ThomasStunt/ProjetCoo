package services;

import java.sql.SQLException;

import model.exceptions.ServiceException;
import model.user.Player;
import persist.dao.DAOPlayer;

public class LogIn {

	public static Player login(String nameLog) throws ServiceException {
		try {
			Player player = DAOPlayer.getInstance().findByUniqueName(nameLog);
			return player;
		} catch (SQLException e) {
			throw new ServiceException("Unable to login. \n Player " + nameLog + " unreachable.", e);
		}
	}

}
