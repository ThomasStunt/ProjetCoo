package services;

import model.exceptions.ActionException;
import model.exceptions.ServiceException;
import model.game.Game;
import model.lands.Land;
import model.user.Player;
import model.user.player.actions.AttackArmyAction;

public class ActionAttack {

	public static void actionAttack(Game game, Player source, Player target, Land land) throws ServiceException {
		Integer cost = game.getSettings().getResourcesCostAction().get(AttackArmyAction.class);
		if (cost == null) {
			cost = 1;
		}
		AttackArmyAction act = new AttackArmyAction(cost, source, target, land);
		try {
			act.doAction();
		} catch (ActionException e) {
			throw new ServiceException("Can't attack.", e);
		}
	}

}
