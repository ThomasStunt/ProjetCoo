package services;

import java.sql.SQLException;
import java.util.List;

import model.exceptions.ServiceException;
import model.game.EndGame;
import persist.dao.DAOPlayer;

public class GetListEndGamePlayer {

	public static List<EndGame> getListEndGamePlayer(String player) throws ServiceException {
		try {
			return DAOPlayer.getInstance().findByUniqueName(player).getEndGames();
		} catch (SQLException e) {
			throw new ServiceException("Unable to reach finish games list of Player " + player + ".", e);
		}
	}

}
