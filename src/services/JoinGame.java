package services;

import java.sql.SQLException;

import model.exceptions.ServiceException;
import model.game.Game;
import model.user.Player;
import persist.dao.DAOPlayer;

public class JoinGame {

	public static void joinGame(String player, Game game) throws ServiceException {
		if (game.getState().getState() == "WAITING") {
			try {
				Player p = DAOPlayer.getInstance().findByUniqueName(player);
				if (game.getSettings().getMaxPlayer() == game.getPlayers().size() + 1) {
					throw new ServiceException("Can't join this game : max players limit reached.");
				}
				game.addPlayer(p);
			} catch (SQLException e) {
				throw new ServiceException("Unable to reach Player " + player + ".", e);
			}
		} else {
			throw new ServiceException("You can't join this game anymore.");
		}

	}

}
