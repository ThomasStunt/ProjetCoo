package services;

import java.util.List;

import model.exceptions.ActionException;
import model.exceptions.ServiceException;
import model.game.Game;
import model.lands.Land;
import model.player.content.Army;
import model.user.Player;
import model.user.player.actions.MoveArmyAction;

public class ActionMove {

	public static void actionMove(Game game, Player player, List<Army> armies, List<Land> course)
			throws ServiceException {
		Integer cost = game.getSettings().getResourcesCostAction().get(MoveArmyAction.class);
		if (cost == null) {
			cost = 1;
		}
		MoveArmyAction act = new MoveArmyAction(cost, player, armies, course);
		try {
			act.doAction();
		} catch (ActionException e) {
			throw new ServiceException("Can't move.", e);
		}
	}

}
