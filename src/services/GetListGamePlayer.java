package services;

import java.sql.SQLException;
import java.util.List;

import model.exceptions.ServiceException;
import model.game.Game;
import persist.dao.DAOPlayer;

public class GetListGamePlayer {

	public static List<Game> getListGamePlayer(String player) throws ServiceException {
		try {
			return DAOPlayer.getInstance().findByUniqueName(player).getGames();
		} catch (SQLException e) {
			throw new ServiceException("Unable to reach Games List of Player " + player + ".", e);
		}
	}

}
