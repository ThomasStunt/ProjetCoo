package services;

import java.sql.SQLException;

import model.exceptions.ServiceException;
import model.user.Player;
import persist.dao.DAOPlayer;

public class CreateNewPlayer {

	public static void createNewPlayer(String name) throws ServiceException {
		Player player = new Player(name, 0);
		try {
			DAOPlayer.getInstance().insert(player);
		} catch (SQLException e) {
			throw new ServiceException("Unable to create new Player " + name + ".", e);
		}
	}

}
