package main;

import java.util.HashMap;

import model.game.Game;
import model.game.Settings;
import model.lands.FieldLand;
import model.lands.Land;
import model.lands.MountainLand;
import model.lands.PlainLand;
import model.user.player.actions.AbstractAction;
import model.user.player.actions.AttackArmyAction;
import model.user.player.actions.CreateArmyAction;
import model.user.player.actions.CreateCityAction;
import model.user.player.actions.MoveArmyAction;
import persist.dao.DAOPlayer;
import services.CreateNewGame;
import view.gui.GameFrame;

public class testGraphique {
	public static void main(String[] args) {
		try {
			Settings s = new Settings();
			s.setSizeX(6);
			s.setSizeY(6);
			s.setDistanceMiniBetweenStartCities(12);
			s.setMaxPlayer(2);
			s.setResourcesRound(150);
			s.setResourcesProductByField(10);
			HashMap<Class<? extends Land>, Float> probs = new HashMap<Class<? extends Land>, Float>();
			probs.put(PlainLand.class, (float) 0.8);
			probs.put(MountainLand.class, (float) 0.10);
			probs.put(FieldLand.class, (float) 0.10);
			s.setProbLandsGeneration(probs);
			HashMap< Class <? extends AbstractAction>, Integer> costActions = new HashMap<Class<? extends AbstractAction>, Integer>();
			costActions.put(AttackArmyAction.class, 5);
			costActions.put(CreateArmyAction.class, 10);
			costActions.put(CreateCityAction.class, 20);
			costActions.put(MoveArmyAction.class, 1);
			s.setResourcesCostAction(costActions);
			Game g = CreateNewGame.createNewGame("test", "Toma", s);
			new GameFrame(g, DAOPlayer.getInstance().findByUniqueName("Toma"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
