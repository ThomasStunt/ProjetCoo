package main;

import view.gui.ConnexionFrame;

/**
 * Launch the software
 *
 */
public class Launch {

	/**
	 * Main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new ConnexionFrame();
	}
}
