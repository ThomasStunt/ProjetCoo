package model.user;

import java.util.ArrayList;
import java.util.List;

import model.IDomainObject;
import model.Visitor;
import model.game.EndGame;
import model.game.Game;

/**
 * Domain object. Represent a player
 *
 */
public class Player extends IDomainObject {

	/**
	 * Name of the Player
	 */
	private String name;

	/**
	 * Friends of the player
	 */
	private List<Player> friends;

	/**
	 * Games hosted by the player
	 */
	private List<Game> hostedGames;

	/**
	 * Games joined by the player
	 */
	private List<Game> games;

	/**
	 * Resources of the player in his current game
	 */
	private Integer ressources;

	/**
	 * List of his ended games
	 */
	private List<EndGame> endGames;

	public Player(final String name, final Integer resources) {
		this.setName(name);
		this.ressources = resources;
		friends = new ArrayList<Player>();
		hostedGames = new ArrayList<Game>();
		games = new ArrayList<Game>();
		endGames = new ArrayList<EndGame>();
	}

	public List<Game> getGames() {
		return games;
	}

	public void addGame(final Game game) {
		games.add(game);
	}

	public void removeHostedGame(final Game game) {
		if (hostedGames.contains(game)) {
			hostedGames.remove(game);
			this.setChanged();
			notifyObservers();
		}
	}

	public void addHostedGame(final Game game) {
		if (!hostedGames.contains(game)) {
			hostedGames.add(game);
			this.setChanged();
			notifyObservers();
		}
	}

	public void addFriend(final Player friend) {
		if (!friends.contains(friend)) {
			friends.add(friend);
			this.setChanged();
			notifyObservers();
		}
	}

	public void removeFriend(final Player friend) {
		if (friends.contains(friend)) {
			friends.remove(friend);
			this.setChanged();
			notifyObservers();
		}
	}

	public void leaveGame(final Game game) {
		games.remove(game);
	}

	public Integer getRessources() {
		return ressources;
	}

	public void setRessources(Integer ressources) {
		this.ressources = ressources;
	}

	public List<EndGame> getEndGames() {
		return endGames;
	}

	public void addEndGames(final EndGame eg) {
		if (!endGames.contains(eg)) {
			endGames.add(eg);
			this.setChanged();
			notifyObservers();
		}
	}

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Player> getFriends() {
		return friends;
	}

	public void setFriends(List<Player> friends) {
		this.friends = friends;
	}

	public List<Game> getHostedGames() {
		return hostedGames;
	}

	public void setHostedGames(List<Game> hostedGames) {
		this.hostedGames = hostedGames;
	}

	public void setGames(List<Game> games) {
		this.games = games;
	}

	public void setEndGames(List<EndGame> endGames) {
		this.endGames = endGames;
	}

}
