package model.user.player.actions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import model.exceptions.ActionException;
import model.lands.Land;
import model.player.content.City;
import model.player.content.Content;
import model.user.Player;

/**
 * Allow to player to create a new city on a land
 *
 */
public class CreateCityAction extends AbstractAction {

	/**
	 * Player source
	 */
	private Player player;

	/**
	 * Land to create a new city
	 */
	private Land land;

	/**
	 * The city name
	 */
	private String cityName;

	public CreateCityAction(int defaultCost, Player player, Land land, String cityName) {
		super(defaultCost);
		this.player = player;
		this.land = land;
		this.cityName = cityName;
	}

	@Override
	public Boolean canExecute() {
		if (player.getRessources() >= this.getDefaultCost() && checkPlayerControl() && checkNoCity()) {
			return true;
		}
		return false;
	}

	@Override
	public void doAction() throws ActionException {
		if (canExecute()) {
			land.addContent(new City(player, cityName));
			player.setRessources(player.getRessources() - this.getDefaultCost());
		} else {
			throw new ActionException("Can't create a city in this location. \n Cause : " + getExceptionCause());
		}
	}

	/**
	 * Check if there is no city on this land
	 *
	 * @return
	 */
	private boolean checkNoCity() {
		List<Content> contents = land.getContents();
		for (Content content : contents) {
			if (content instanceof City) {
				setExceptionCause("Already a city in this land.");
				return false;
			}
		}
		return true;
	}

	/**
	 * Check if the player have the control of this land
	 * 
	 * @return
	 */
	private boolean checkPlayerControl() {
		boolean flag = true;
		HashMap<UUID, Integer> armyNumberById = new HashMap<>();
		List<Content> contents = land.getContents();
		for (Content content : contents) {
			UUID id_player = content.getOwner().getId();
			if (armyNumberById.containsKey(id_player)) {
				Integer nbArmies = armyNumberById.get(id_player);
				armyNumberById.put(id_player, nbArmies + 1);
			} else {
				armyNumberById.put(id_player, 1);
			}
		}
		Integer nbArmiesPlayer = armyNumberById.get(player.getId());
		for (Map.Entry<UUID, Integer> entry : armyNumberById.entrySet()) {
			if (!entry.getKey().equals(player.getId())) {
				if (entry.getValue() >= nbArmiesPlayer) {
					flag = false;
					setExceptionCause("Player " + player.getName() + " doesn't have the control of this land.");
				}
			}
		}
		return flag;
	}

}
