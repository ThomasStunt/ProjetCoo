package model.user.player.actions;

import model.exceptions.ActionException;

/**
 * Super class for actions in game
 *
 */
public abstract class AbstractAction {

	/**
	 * Default cost of the action
	 */
	private int defaultCost;

	/**
	 * If an error occured, describe the cause
	 */
	private String exceptionCause;

	/**
	 *
	 * @return true if this action can be exec by a player, false if it can't be
	 */
	public abstract Boolean canExecute();

	/**
	 * Exec the action by a player
	 * 
	 * @throws ActionException
	 */
	public abstract void doAction() throws ActionException;

	public int getDefaultCost() {
		return defaultCost;
	}

	public AbstractAction(final int defaultCost) {
		this.defaultCost = defaultCost;
		this.setExceptionCause("Insufficient resources.");
	}

	public String getExceptionCause() {
		return exceptionCause;
	}

	public void setExceptionCause(String exceptionCause) {
		this.exceptionCause = exceptionCause;
	}

}
