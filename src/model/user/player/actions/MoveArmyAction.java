package model.user.player.actions;

import java.util.List;

import model.exceptions.ActionException;
import model.lands.Land;
import model.lands.MountainLand;
import model.player.content.Army;
import model.user.Player;

/**
 * Action to move an army
 *
 */
public class MoveArmyAction extends AbstractAction {

	/**
	 * Player source
	 */
	private Player player;

	/**
	 * List of armies to move
	 */
	private List<Army> armies;

	/**
	 * The course to do
	 */
	private List<Land> course;

	/**
	 * The cost of the course
	 */
	private Integer cost;

	public MoveArmyAction(final int defaultCost, Player player, List<Army> armies, List<Land> course) {
		super(defaultCost);
		this.player = player;
		this.armies = armies;
		this.course = course;
	}

	/**
	 * Check if armies selected belongs to the player
	 *
	 * @return
	 */
	private boolean checkListArmyPlayer() {
		for (Army army : armies) {
			if (!army.getOwner().getId().equals(player.getId())) {
				setExceptionCause("An army doesn't belong to Player " + player.getName());
				return false;
			}
			if (!course.get(0).getContents().contains(army)) {
				setExceptionCause("An army doesn't belong to departure land. " + player.getName());
				return false;
			}
		}
		return true;
	}

	/**
	 * Check if the course is possible
	 *
	 * @return
	 */
	private boolean checkCourse() {
		int tmp_x = course.get(0).getX();
		int tmp_y = course.get(0).getY();
		for (Land land : course) {
			if (!(land.getX() == tmp_x && land.getY() == tmp_y)) {
				// HAUT x,y+ land.getX() == tmp_x && land.getY() == tmp_y + 1
				// BAS x,y- land.getX() == tmp_x && land.getY() == tmp_y - 1
				// GAUCHE x-,y land.getX() == tmp_x - 1 && land.getY() == tmp_y
				// DROITE x+,y land.getX() == tmp_x + 1 && land.getY() == tmp_y
				if (!((land.getX() == tmp_x && land.getY() == tmp_y + 1)
						|| (land.getX() == tmp_x && land.getY() == tmp_y - 1)
						|| (land.getX() == tmp_x - 1 && land.getY() == tmp_y)
						|| (land.getX() == tmp_x + 1 && land.getY() == tmp_y))) {
					setExceptionCause("Course not possible.");
					return false;
				}
			}
			tmp_x = land.getX();
			tmp_y = land.getY();
		}
		return true;
	}

	/**
	 * Calcul the cost of the course
	 * 
	 * @return
	 */
	private Integer calculCost() {
		Integer normalDistance = 0;
		Integer mountainDistance = 0;
		for (Land land : course) {
			if (land instanceof MountainLand) {
				mountainDistance++;
			} else {
				normalDistance++;
			}
		}
		Integer cost = getDefaultCost() * normalDistance + (getDefaultCost() * mountainDistance) * 2;
		this.cost = cost;
		return cost;
	}

	@Override
	public Boolean canExecute() {
		if (player.getRessources() >= calculCost() && checkListArmyPlayer() && checkCourse()) {
			return true;
		}
		return false;
	}

	@Override
	public void doAction() throws ActionException {
		if (canExecute()) {
			Land departure = course.get(0);
			Land target = course.get(course.size() - 1);
			for (Army army : armies) {
				departure.removeContent(army);
				target.addContent(army);
			}
			player.setRessources(player.getRessources() - this.cost);
		} else {
			throw new ActionException("Can't move. \n Cause : " + getExceptionCause());
		}
	}

}
