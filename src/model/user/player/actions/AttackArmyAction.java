package model.user.player.actions;

import java.util.List;

import model.exceptions.ActionException;
import model.lands.Land;
import model.player.content.Army;
import model.player.content.City;
import model.player.content.Content;
import model.user.Player;

/**
 * Action of a player to attack with his army a city
 *
 */
public class AttackArmyAction extends AbstractAction {

	/**
	 * Player that do this action
	 */
	private Player source;

	/**
	 * Player targeted
	 */
	private Player target;

	/**
	 * Land source
	 */
	private Land land;

	/**
	 * The city targeted
	 */
	private City cityTargeted;

	public AttackArmyAction(final int defaultCost, Player source, Player target, Land land) {
		super(defaultCost);
		this.source = source;
		this.target = target;
		this.land = land;
	}

	@Override
	public Boolean canExecute() {
		if (source.getRessources() >= this.getDefaultCost() && checkCity() && checkArmyPower()) {
			return true;
		}
		return false;
	}

	/**
	 * Check if the player army is sufficient to reach the goal of this action
	 *
	 * @return true if the army is sufficient, or false
	 */
	private boolean checkArmyPower() {
		List<Content> contents = land.getContents();
		Integer nbSource = 0;
		Integer nbTarget = 0;
		for (Content content : contents) {
			if (content instanceof Army) {
				if (content.getOwner().getId().equals(source.getId())) {
					nbSource++;
				}
				if (content.getOwner().getId().equals(target.getId())) {
					nbTarget++;
				}
			}
		}
		if (nbSource > nbTarget * 2) {
			return true;
		}
		setExceptionCause(
				"Player + " + source.getName() + "'s armies no sufficient to attack Player " + target.getName());
		return false;
	}

	/**
	 * Check if there is a city on the land
	 *
	 * @return true if there is a city of the player targeted, or false
	 */
	private boolean checkCity() {
		List<Content> contents = land.getContents();
		for (Content content : contents) {
			if (content instanceof City) {
				City city = (City) content;
				if (city.getOwner().getId().equals(target.getId())) {
					cityTargeted = (City) content;
					return true;
				}
			}
		}
		setExceptionCause("Player " + target.getName() + " doesn't have a city on this land.");
		return false;
	}

	/**
	 * If can execute, destroy the city of the player targeted and remove resources
	 * cost
	 */
	@Override
	public void doAction() throws ActionException {
		if (canExecute()) {
			land.removeContent(cityTargeted);
			source.setRessources(source.getRessources() - this.getDefaultCost());
		} else {
			throw new ActionException("Can't attack a city in this location. \n Cause : " + getExceptionCause());
		}
	}

}
