package model.user.player.actions;

import java.util.List;

import model.exceptions.ActionException;
import model.lands.Land;
import model.player.content.Army;
import model.player.content.City;
import model.player.content.Content;
import model.user.Player;

/**
 * Allow to the player to create a new army
 *
 */
public class CreateArmyAction extends AbstractAction {

	/**
	 * Player source
	 */
	private Player player;

	/**
	 * Land to add an army
	 */
	private Land land;

	/**
	 * Name of the new army
	 */
	private String armyName;

	public CreateArmyAction(int defaultCost, Player player, Land land, String armyName) {
		super(defaultCost);
		this.player = player;
		this.land = land;
		this.armyName = armyName;
	}

	@Override
	public Boolean canExecute() {
		if (player.getRessources() >= this.getDefaultCost() && checkCity()) {
			return true;
		}
		return false;
	}

	/**
	 * Check if the player have a city on this land
	 *
	 * @return true if there is a city on this land, or false
	 */
	private boolean checkCity() {
		List<Content> contents = land.getContents();
		for (Content content : contents) {
			if (content instanceof City) {
				City city = (City) content;
				if (city.getOwner().getId().equals(player.getId())) {
					return true;
				}
			}
		}
		setExceptionCause("Player " + player.getName() + " doesn't have a city on this land.");
		return false;
	}

	/**
	 * If it can ba execute, create a new army on the land and remove the resources
	 * cost
	 */
	@Override
	public void doAction() throws ActionException {
		if (canExecute()) {
			land.addContent(new Army(player, armyName));
			player.setRessources(player.getRessources() - this.getDefaultCost());
		} else {
			throw new ActionException("Can't create an army in this location. \n Cause : " + getExceptionCause());
		}
	}

}
