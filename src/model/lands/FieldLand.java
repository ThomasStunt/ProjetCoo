package model.lands;

import model.Visitor;

public class FieldLand extends Land {

	public FieldLand(int x, int y) {
		super(x, y);
	}

	@Override
	public String getType() {
		return "FIELD";
	}

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}

}
