package model.lands;

import model.Visitor;

public class PlainLand extends Land {

	public PlainLand(int x, int y) {
		super(x, y);
	}

	@Override
	public String getType() {
		return "PLAIN";
	}

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}
}
