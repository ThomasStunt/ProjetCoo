package model.lands;

import java.util.ArrayList;
import java.util.List;

import model.IDomainObject;
import model.player.content.Content;

/**
 * Domain object. Abstract class for land type
 *
 */
public abstract class Land extends IDomainObject {

	/**
	 * Location x, y
	 */
	private int x, y;

	/**
	 * Contents of this land
	 */
	private List<Content> contents;

	public Land(final int x, final int y) {
		super();
		this.x = x;
		this.y = y;
		this.contents = new ArrayList<>();
	}

	public abstract String getType();

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void addContent(Content content) {
		contents.add(content);
		notifyObservers();
	}

	public void removeContent(Content content) {
		contents.remove(content);
		notifyObservers();
	}

	public List<Content> getContents() {
		return contents;
	}

	public void setContents(List<Content> contents) {
		this.contents = contents;
	}

}
