package model.lands;

import model.Visitor;

public class MountainLand extends Land {

	public MountainLand(int x, int y) {
		super(x, y);
	}

	@Override
	public String getType() {
		return "MOUNTAIN";
	}

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}

}
