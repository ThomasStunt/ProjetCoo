package model.game;

import java.util.ArrayList;
import java.util.List;

import model.IDomainObject;
import model.Visitor;
import model.game.round.Round;
import model.game.state.GameState;
import model.game.state.WaitingState;
import model.user.Player;

/**
 * Domain object. Represent a game.
 *
 */
public class Game extends IDomainObject {

	/**
	 * Name of the game
	 */
	private String name;

	/**
	 * Host player
	 */
	private Player host;

	/**
	 * List of all players (except host)
	 */
	private List<Player> players;

	/**
	 * Current state of the game
	 */
	private GameState state;

	/**
	 * Gameboard of the game
	 */
	private GameBoard gameBoard;

	/**
	 * Settings of the game
	 */
	private Settings settings;

	/**
	 * Current round of the game
	 */
	private Round round;

	public Game(final String name, final Player host, final int xmap, final int ymap) {
		this.name = name;
		players = new ArrayList<Player>();
		state = new WaitingState();
		this.host = host;
	}

	public Game(final String name, final Player host, Settings set) {
		this.name = name;
		players = new ArrayList<Player>();
		state = new WaitingState();
		this.host = host;
		this.settings = set;
	}

	/**
	 * Add a player in the list (if possible). Notify observers when it's done
	 *
	 * @param player
	 */
	public void addPlayer(Player player) {
		if (!(player.equals(host) || players.contains(player))) {
			players.add(player);
			this.setChanged();
			this.notifyObservers();
		}
	}

	public void setState(GameState state) {
		this.state = state;
		this.setChanged();
		this.notifyObservers();
	}

	public Player getHost() {
		return host;
	}

	public void setHost(Player host) {
		this.host = host;
	}

	public String getName() {
		return name;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public GameState getState() {
		return state;
	}

	public GameBoard getGameBoard() {
		return gameBoard;
	}

	public Settings getSettings() {
		return settings;
	}

	public void setSettings(Settings settings) {
		this.settings = settings;
	}

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public void setGameBoard(GameBoard gameBoard) {
		this.gameBoard = gameBoard;
		this.setChanged();
		this.notifyObservers();
	}

	public Round getRound() {
		return round;
	}

	public void setRound(Round round) {
		this.round = round;
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public String toString() {
		return "Nom : " + this.getName();
	}

}
