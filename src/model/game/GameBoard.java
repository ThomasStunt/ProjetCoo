package model.game;

import java.util.List;

import model.IDomainObject;
import model.Visitor;
import model.lands.Land;

/**
 * Domain object. Gameboard of a game
 *
 */
public class GameBoard extends IDomainObject {

	/**
	 * 2 dimensions tab of Land
	 */
	private Land[][] map;

	/**
	 * Abs size
	 */
	private Integer sizeX;

	/**
	 * Ord size
	 */
	private Integer sizeY;

	public GameBoard(final Settings settings) {
		super();
		this.map = new Land[settings.getSizeX()][settings.getSizeY()];
		setSizeX(settings.getSizeX());
		setSizeY(settings.getSizeY());
	}

	public GameBoard() {
		super();
		this.map = new Land[25][25];
		setSizeX(25);
		setSizeY(25);
	}

	public Land[][] getMap() {
		return map;
	}

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}

	public Integer getSizeX() {
		return sizeX;
	}

	public void setSizeX(Integer sizeX) {
		this.sizeX = sizeX;
	}

	public Integer getSizeY() {
		return sizeY;
	}

	public void setSizeY(Integer sizeY) {
		this.sizeY = sizeY;
	}

	public void setMap(Land[][] map) {
		this.map = map;
		this.setChanged();
		this.notifyObservers();
	}

	public void setMap(List<Land> map) {
		for (Land land : map) {
			this.map[land.getX()][land.getY()] = land;
		}
		this.setChanged();
		this.notifyObservers();
	}

}
