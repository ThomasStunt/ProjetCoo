package model.game.round;

import java.util.Iterator;
import java.util.List;

import model.IDomainObject;
import model.Visitor;
import model.user.Player;

/**
 * Domain object. Iterator of players (for round)
 *
 */
public class IteratListPlayer extends IDomainObject implements Iterator<Player> {

	private List<Player> players;

	private int index;

	public IteratListPlayer(List<Player> players) {
		this.players = players;
		index = 0;
	}

	public void reset() {
		index = 0;
	}

	@Override
	public boolean hasNext() {
		try {
			if (players.get(index) != null) {
				return true;
			}
		} catch (IndexOutOfBoundsException e) {
			return false;
		}
		return false;
	}

	@Override
	public Player next() {
		try {
			Player p = players.get(index);
			index++;
			return p;
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

}
