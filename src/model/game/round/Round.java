package model.game.round;

import model.IDomainObject;
import model.Visitor;

/**
 * Domain object. Use to do round in game
 *
 */
public class Round extends IDomainObject {

	/**
	 * Players playing iterator
	 */
	private IteratListPlayer playersOrder;

	/**
	 * Number of rounds done
	 */
	private Integer numberRound;

	public Round(final Integer numberRound, final IteratListPlayer playersOrder) {
		this.playersOrder = playersOrder;
		this.numberRound = numberRound;
	}

	public IteratListPlayer getPlayersOrder() {
		return playersOrder;
	}

	public Integer getNumberRound() {
		return numberRound;
	}

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}

	public void setPlayersOrder(IteratListPlayer playersOrder) {
		this.playersOrder = playersOrder;
	}

	public void setNumberRound(Integer numberRound) {
		this.numberRound = numberRound;
		this.setChanged();
		this.notifyObservers();
	}

}
