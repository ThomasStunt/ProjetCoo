package model.game.state;

import model.game.Game;

/**
 * Abstract class of game state
 *
 */
public abstract class GameState {

	/**
	 *
	 * @return current state
	 */
	public abstract String getState();

	/**
	 * Change the state of the game for this state
	 *
	 * @param g
	 */
	public abstract void changeState(Game g);

	/**
	 * Change the state of the game for the next one
	 * 
	 * @param g
	 */
	public abstract void changeToNextState(Game g);
}
