package model.game.state;

import model.game.Game;

/**
 * State when the game is created and waiting for all players to began
 */
public class WaitingState extends GameState {

	@Override
	public void changeState(Game g) {
		g.setState(new WaitingState());
	}

	@Override
	public String getState() {
		return "WAITING";
	}

	@Override
	public void changeToNextState(Game g) {
		g.setState(new RunningState());
	}

}
