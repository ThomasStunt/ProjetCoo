package model.game.state;

import model.game.Game;

/**
 * State when the game is currently running
 */
public class RunningState extends GameState {

	@Override
	public void changeState(Game g) {
		g.setState(new RunningState());
	}

	@Override
	public String getState() {
		return "RUNNING";
	}

	@Override
	public void changeToNextState(Game g) {
		g.setState(new PauseState());
	}

}
