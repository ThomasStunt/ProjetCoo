package model.game.state;

import model.game.Game;

/**
 * State when the HostPlayer is leaving a running game.
 */

public class PauseState extends GameState {

	@Override
	public void changeState(Game g) {
		g.setState(new PauseState());
	}

	@Override
	public String getState() {
		return "PAUSE";
	}

	@Override
	public void changeToNextState(Game g) {
		g.setState(new RunningState());
	}

}
