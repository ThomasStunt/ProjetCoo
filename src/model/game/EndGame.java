package model.game;

import java.util.List;

import model.IDomainObject;
import model.Visitor;

/**
 * Domain object. Represent saved data of an ended game.
 *
 */
public class EndGame extends IDomainObject {

	/**
	 * The name of the game
	 */
	private String gameName;

	/**
	 * Name of the host player
	 */
	private String hostPlayer;

	/**
	 * List of all players (except host)
	 */
	private List<String> players;

	/**
	 * Number of rounds used to finish the game
	 */
	private int nbRounds;

	/**
	 * Name of the winner
	 */
	private String winnerName;

	/**
	 * Constructor
	 *
	 * @param gameName
	 * @param hostPlayer
	 * @param players
	 * @param nbRounds
	 * @param winnerName
	 */
	public EndGame(String gameName, String hostPlayer, List<String> players, int nbRounds, String winnerName) {
		super();
		this.gameName = gameName;
		this.hostPlayer = hostPlayer;
		this.players = players;
		this.nbRounds = nbRounds;
		this.winnerName = winnerName;
	}

	public String getHostPlayer() {
		return hostPlayer;
	}

	public List<String> getPlayers() {
		return players;
	}

	public int getNbRounds() {
		return nbRounds;
	}

	public String getWinnerName() {
		return winnerName;
	}

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}

	public void setHostPlayer(String hostPlayer) {
		this.hostPlayer = hostPlayer;
	}

	public void setPlayers(List<String> players) {
		this.players = players;
	}

	public void setNbRounds(int nbRounds) {
		this.nbRounds = nbRounds;
	}

	public void setWinnerName(String winnerName) {
		this.winnerName = winnerName;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	/**
	 *
	 * @return the description
	 */
	public String getDescription() {
		String descr = new String("GameName : " + gameName + "\nHost : " + hostPlayer + "\nPlayers : [");
		int index = 0;
		for (String string : players) {
			if (index == players.size() - 1) {
				descr = descr.concat(string);
			} else {
				descr = descr.concat(string + ",");
			}
			index++;
		}
		descr = descr.concat("]\nRounds : " + nbRounds);
		descr = descr.concat("\nWinner : " + winnerName);
		return descr;
	}

	/**
	 *
	 * @return the description in HTML balises
	 */
	public String getDescriptionHTML() {
		String descr = new String("GameName : " + gameName + "<br/>Host : " + hostPlayer + "<br/>Players : [");
		int index = 0;
		for (String string : players) {
			if (index == players.size() - 1) {
				descr = descr.concat(string);
			} else {
				descr = descr.concat(string + ",");
			}
			index++;
		}
		descr = descr.concat("]<br/>Rounds : " + nbRounds);
		descr = descr.concat("<br/>Winner : " + winnerName);
		return descr;
	}

	@Override
	public String toString() {
		return this.gameName + " - " + this.winnerName;
	}

}
