package model.game;

import java.util.HashMap;

import model.IDomainObject;
import model.Visitor;
import model.lands.Land;
import model.user.player.actions.AbstractAction;

/**
 * Domain object. Settings of a game
 *
 */
public class Settings extends IDomainObject {

	/**
	 * Number of max players
	 */
	private Integer maxPlayer;

	/**
	 * Size x of the map
	 */
	private Integer sizeX;

	/**
	 * Size y of the map
	 */
	private Integer sizeY;

	/**
	 * Number of resources to give each round
	 */
	private Integer resourcesRound;

	/**
	 * Probability of generation of lands
	 */
	private HashMap<Class<? extends Land>, Float> probLandsGeneration;

	/**
	 * Default cost of actions
	 */
	private HashMap<Class<? extends AbstractAction>, Integer> resourcesCostAction;

	/**
	 * Number of resources give by field
	 */
	private Integer resourcesProductByField;

	/**
	 * Minimum distance between cities
	 */
	private Integer distanceMiniBetweenStartCities;

	public Integer getMaxPlayer() {
		return maxPlayer;
	}

	public void setMaxPlayer(Integer maxPlayer) {
		this.maxPlayer = maxPlayer;
	}

	public Integer getSizeX() {
		return sizeX;
	}

	public void setSizeX(Integer sizeX) {
		this.sizeX = sizeX;
	}

	public Integer getSizeY() {
		return sizeY;
	}

	public void setSizeY(Integer sizeY) {
		this.sizeY = sizeY;
	}

	public Integer getResourcesRound() {
		return resourcesRound;
	}

	public void setResourcesRound(Integer resourcesRound) {
		this.resourcesRound = resourcesRound;
	}

	public HashMap<Class<? extends Land>, Float> getProbLandsGeneration() {
		return probLandsGeneration;
	}

	public void setProbLandsGeneration(HashMap<Class<? extends Land>, Float> probLandsGeneration) {
		this.probLandsGeneration = probLandsGeneration;
	}

	public Integer getResourcesProductByField() {
		return resourcesProductByField;
	}

	public void setResourcesProductByField(Integer resourcesProductByField) {
		this.resourcesProductByField = resourcesProductByField;
	}

	public Integer getDistanceMiniBetweenStartCities() {
		return distanceMiniBetweenStartCities;
	}

	public void setDistanceMiniBetweenStartCities(Integer distanceMiniBetweenStartCities) {
		this.distanceMiniBetweenStartCities = distanceMiniBetweenStartCities;
	}

	public HashMap<Class<? extends AbstractAction>, Integer> getResourcesCostAction() {
		return resourcesCostAction;
	}

	public void setResourcesCostAction(HashMap<Class<? extends AbstractAction>, Integer> resourcesCostAction) {
		this.resourcesCostAction = resourcesCostAction;
	}

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}

}
