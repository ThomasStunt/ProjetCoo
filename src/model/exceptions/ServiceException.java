package model.exceptions;

/**
 * Exception to throw when an error occured in a service.
 *
 */
public class ServiceException extends Exception {

	private static final long serialVersionUID = 523603408254623706L;

	public ServiceException() {
		super();
	}

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}

}
