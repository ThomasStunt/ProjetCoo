package model.exceptions;

/**
 * Exception to throw when an error occured in an action.
 *
 */
public class ActionException extends Exception {

	private static final long serialVersionUID = -4652117166710274601L;

	public ActionException() {
		super();
	}

	public ActionException(String message) {
		super(message);
	}

	public ActionException(String message, Throwable cause) {
		super(message, cause);
	}

	public ActionException(Throwable cause) {
		super(cause);
	}

}
