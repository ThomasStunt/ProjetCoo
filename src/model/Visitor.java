package model;

import model.game.EndGame;
import model.game.Game;
import model.game.GameBoard;
import model.game.Settings;
import model.game.round.IteratListPlayer;
import model.game.round.Round;
import model.lands.Land;
import model.player.content.Content;
import model.user.Player;

/**
 * Pattern Visitor
 *
 */
public abstract class Visitor {

	/**
	 * Visit an IDomainObject
	 *
	 * @param o
	 */
	public void visit(IDomainObject o) {
		o.accept(this);
	}

	/**
	 * Process performed for an {@link IDomainObject} {@link Player}
	 *
	 * @param p
	 */
	abstract public void visit(Player p);

	/**
	 * Process performed for an {@link IDomainObject} {@link EndGame}
	 *
	 * @param p
	 */
	abstract public void visit(EndGame eg);

	/**
	 * Process performed for an {@link IDomainObject} {@link Game}
	 *
	 * @param p
	 */
	abstract public void visit(Game g);

	/**
	 * Process performed for an {@link IDomainObject} {@link GameBoard}
	 *
	 * @param p
	 */
	abstract public void visit(GameBoard gb);

	/**
	 * Process performed for an {@link IDomainObject} {@link Settings}
	 *
	 * @param p
	 */
	abstract public void visit(Settings s);

	/**
	 * Process performed for an {@link IDomainObject} {@link IteratListPlayer}
	 *
	 * @param p
	 */
	abstract public void visit(IteratListPlayer ilp);

	/**
	 * Process performed for an {@link IDomainObject} {@link Round}
	 *
	 * @param p
	 */
	abstract public void visit(Round r);

	/**
	 * Process performed for an {@link IDomainObject} {@link Land}
	 *
	 * @param p
	 */
	abstract public void visit(Land l);

	/**
	 * Process performed for an {@link IDomainObject} {@link Content}
	 *
	 * @param p
	 */
	abstract public void visit(Content c);

}