package model;

import java.util.Observable;
import java.util.UUID;

/**
 * Abstract class for all domain objects. Observable and Visitable
 *
 */
public abstract class IDomainObject extends Observable implements Visitable {

	/**
	 * Object's ID
	 */
	private UUID id;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

}
