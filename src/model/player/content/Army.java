package model.player.content;

import model.Visitor;
import model.user.Player;

public class Army extends Content {

	public Army(final Player owner, final String name) {
		super(owner, name);
	}

	@Override
	public String getType() {
		return "ARMY";
	}

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}

}
