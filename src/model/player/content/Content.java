package model.player.content;

import model.IDomainObject;
import model.user.Player;

/**
 * Domain object. Abstract class for contents that can be put in lands
 */
public abstract class Content extends IDomainObject {

	/**
	 * Player owner of this content
	 */
	private Player owner;

	/**
	 * Name of this content
	 */
	private String contentName;

	public Content(Player owner, String contentName) {
		super();
		this.owner = owner;
		this.contentName = contentName;
	}

	public abstract String getType();

	public String getContentName() {
		return contentName;
	}

	public Player getOwner() {
		return owner;
	}

}
