package model.player.content;

import model.Visitor;
import model.user.Player;

public class City extends Content {

	public City(final Player owner, final String name) {
		super(owner, name);
	}

	@Override
	public String getType() {
		return "CITY";
	}

	@Override
	public void accept(Visitor v) {
		v.visit(this);
	}

}
