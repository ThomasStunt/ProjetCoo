package model;

/**
 * Pattern Visitor
 *
 */
public interface Visitable {

	/**
	 * Accept the Visitor in args
	 *
	 * @param v
	 */
	void accept(Visitor v);

}
